<?php
/*
    docMaker PHP
    Copyright (C) 2018  Soni TOURRET

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
ORDRE DE RECHERCHE DU CHERCHEUR D'OBJETS :
1. $_documentedClasses
2. $_documentedEnums
3. $_documentedInterfaces
4. $_documentedTypedefs

ORDRE DE RECHERCHE DU CHERCHEUR D'OBJETS POUR LES THROWS :
1. $_documentedClasses
2. $_documentedTypedefs

ORDRE DE RECHERCHE DU CHERCHEUR D'OBJETS POUR LES TYPES :
1. $_documentedClasses
2. $_documentedEnums
3. $_documentedTypedefs

ORDRE DE RECHERCHE DU CHERCHEUR D'OBJETS POUR LES EXTENDS :
1. $_documentedClasses
2. $_documentedInterfaces
3. $_documentedTypedefs

ORDRE DE RECHERCHE DU CHERCHEUR D'OBJETS POUR LES IMPLEMENTS :
1. $_documentedInterfaces
2. $_documentedTypedefs

ORDRE DE RECHERCHE DU CHERCHEUR D'OBJETS POUR LES ANNOTATIONS :
1. $_documentedAnnotations
*/ ?>
<?php

/*
FORMAT :

{
	"name": string default "",
	"description": string default "",
	"version": string default "",
	"author": string default "",
	"packages": [
		{
			"name": string,
			"description": string default null,
			"since": string default "",
			"functions": [
				{
					"name": string,
					"return": string,
					"ref": bool default false,
					"pointer": bool default false,
					"args": [
						{
							"name": string,
							"type": string,
							"const": bool default false,
							"ref": bool default false,
							"pointer": bool default false,
							"default": string default null,
							"description": string default null
						},
						...
					] default null,
					"shortDescription": string default "",
					"description": string default "",
					"since": string default "",
					"deprecated": bool default false,
					"author": string default "",
					"annotations": [
						string | [string, ...], // si tableau : index 0 = nom de l'annotation, autres index = valeur des arguments | si string : nom de l'annotation
						...
					] default null,
					"throws": [
						{
							"name": string,
							"description": string default ""
						},
						...
					] default null,
					"template": bool default false,
					"template_args": [
						{
							"name": string,
							"description": string default "",
							"default": string default ""
						},
						...
					] default null
				},
				...
			] default null,
			"classes": [
				{
					"name": string,
					"abstract": bool default false,
					"final": bool default false,
					"extends": [
						string,
						...
					] default null,
					"implements": [
						string,
						...
					] default null,
					"members": [
						{
							"name": string,
							"scope": "public"|"private"|"protected" default "public",
							"type": string,
							"const": bool default false,
							"static": bool default false,
							"pointer": bool default false,
							"final": bool default false,
							"description": string default null,
							"since": string default null,
							"deprecated": bool default false,
							"author": string default null,
							"annotations": [
								string | [string, ...],
								...
							] default null
						},
						...
					] default null,
					"methods": [
						{
							"name": string,
							"return": string,
							"ref": bool default false,
							"pointer": bool default false,
							"abstract": bool default false,
							"static": bool default false,
							"args": [
								{
									"name": string,
									"type": string,
									"const": bool default false,
									"ref": bool default false,
									"pointer": bool default false,
									"default": string default null,
									"description": string default null
								},
								...
							] default null,
							"shortDescription": string default null,
							"description": string default null,
							"since": string default null,
							"deprecated": false default false,
							"author": string default null,
							"scope": "public"|"private"|"protected" default "public",
							"annotations": [
								string | [string, ...],
								...
							] default null,
							"constructor": bool default false,
							"destructor": bool default false,
							"throws": [
								{
									"name": string,
									"description": string default ""
								}
							] default null
						},
						...
					] default null,
					"shortDescription": string default "",
					"description": string default "",
					"since": string default "",
					"deprecated": bool default false,
					"author": string default "",
					"annotations": [
						string | [string, ...],
						...
					] default null,
				},
				...
			] default null,
			"interfaces": [
				{
					"name": string,
					"methods": [
						{
							"name": string,
							"return": string,
							"ref": bool default false,
							"pointer": bool default false,
							"args": [
								{
									"name": string,
									"type": string,
									"const": bool default false,
									"ref": bool default false,
									"pointer": bool default false,
									"default": string default null,
									"description": string default null
								},
								...
							] default null,
							"shortDescription": string default "",
							"description": string default "",
							"since": string default "",
							"deprecated": false default false,
							"author": string default "",
							"scope": "public"|"private"|"protected" default "public",
							"annotations": [
								string | [string, ...],
								...
							] default null,
							"constructor": bool default false,
							"destructor": bool default false,
							"abstract": bool default false,
							"static": bool default false
						},
						...
					] default null,
					"members": [
						{
							"name": string,
							"scope": "public"|"private"|"protected" default "public",
							"type": string,
							"const": bool default false,
							"static": bool default false,
							"pointer": bool default false,
							"final": bool default false,
							"description": string default null,
							"since": string default null,
							"deprecated": bool default false,
							"author": string default null,
							"annotations": [
								string | [string, ...],
								...
							] default null
						},
						...
					] default null,
					"description": string default null,
					"shortDescription": string default null,
					"author": string default null,
					"since": string default null,
					"deprecated": bool default false,
					"annotations": [
						string | [string, ...],
						...
					] default null
				},
				...
			] default null,
			"variables": [
				{
					"name": string,
					"type": string,
					"pointer": bool default false,
					"const": bool default false,
					"description": string default "",
					"author": string default "",
					"since": string default "",
					"deprecated": bool default false,
					"annotations": [
						string | [string, ...],
						...
					] default null
				},
				...
			] default null,
			"annotations": [
				{
					"name": string,
					"shortDescription": string default "",
					"description": string default "",
					"author": string default "",
					"since": string default "",
					"deprecated": bool default false,
					"args": [
						{
							"name": string,
							"type": string,
							"description": string default null,
							"default": string default null
						},
						...
					] default null
				},
				...
			] default null,
			"preprocessorConstants": [
				{
					"name": string,
					"value": string default "",
					"description": string default "",
					"author": string default "",
					"since": string default "",
					"deprecated": bool default false
				},
				...
			] default null,
			"suitcases": [
				{
					"name": string,
					"shortDescription": string default "",
					"description": string default "",
					"author": string default "",
					"since": string default "",
					"deprecated": bool default false,
					"content": [
						{
							"name": string,
							"value": string default null,
							"description": string default "",
							"since": string default ""
						},
						...
					] default null
				},
				...
			] default null,
			"typedefs": [
				{
					"name": string,
					"value": string,
					"description": string default "",
					"author": string default "",
					"since": string default "",
					"deprecated": bool default false
				},
				...
			] default null
		},
		...
	]
}
*/

class FileNotFoundException       extends Exception {}
class MalformedDataException      extends Exception {}
class IllegalBlankStringException extends Exception {}

$_apptitle                        = "docMaker";
$_appversion                      = "1.0.0";
$_filesCreatedCount               = 0;
$_DESCRMAXLEN                     = 100;
$_DEFAULTSCOPE                    = "public";
$_ANNOTATIONS_PREFIX              = '@';
$lst                              = [];

function help()
{
	fputs(STDOUT, "docMaker.php - version $_appversion\r\n\r\n");
	fputs(STDOUT, "    Syntaxe :\r\n        docmaker <input> [output_dir] [-zip]\r\n\r\n");
}

function check_data(&$data)
{
	if (property_exists($data, "name")) {
		if (!is_string($data->name) && $data->name != NULL)
			throw new MalformedDataException("ROOT.name : string or null expected");
	}
	
	if (property_exists($data, "description")) {
		if (!is_string($data->description) && $data->description != NULL)
			throw new MalformedDataException("ROOT.description : string or null expected");
	}
	
	if (property_exists($data, "version")) {
		if (!is_string($data->version) && $data->version != NULL)
			throw new MalformedDataException("ROOT.version : string or null expected");
	}
	
	if (property_exists($data, "author")) {
		if (!is_string($data->author) && $data->author != NULL)
			throw new MalformedDataException("ROOT.author : string or null expected");
	}
	
	// LES PACKAGES
	if (property_exists($data, "packages")) {
		if (!is_array($data->packages))
			throw new MalformedDataException("ROOT.packages : array expected");
		
		$packagesCount = 0;
		foreach ($data->packages as $packagesIterator) {
			if (!is_object($packagesIterator))
				throw new MalformedDataException("ROOT.packages[" . $packagesCount . "] : object expected");
			else {
				if (property_exists($packagesIterator, "name")) {
					if (!is_string($packagesIterator->name))
						throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].name : string expected");
				} else
					throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].name : mandatory param");
				
				if (property_exists($packagesIterator, "description")) {
					if (!is_string($packagesIterator->description) && $packagesIterator->description != NULL)
						throw new MalformedDataException("ROOT.packages[$packagesCount].description : string or null expected");
				} else
					$data->packages[$packagesCount]->description = NULL;
				
				if (property_exists($packagesIterator, "since")) {
					if (!is_string($packagesIterator->since) && $packagesIterator->since != NULL)
						throw new MalformedDataException("ROOT.packages[$packagesCount].since : string or null expected");
				}
				
				// LES FONCTIONS DANS LES PACKAGES
				if (property_exists($packagesIterator, "functions")) {
					if (!is_array($packagesIterator->functions) && $packagesIterator != NULL)
						throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions : array or null expected");
					
					$functionsCount = 0;
					foreach ($packagesIterator->functions as $functionsIterator) {
						if (!is_object($functionsIterator))
							throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "] : object expected");
						else {
							if (property_exists($functionsIterator, "name")) {
								if (!is_string($functionsIterator->name))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].name : string expected");
							} else
								throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].name : mandatory param");
							
							if (property_exists($functionsIterator, "return")) {
								if (!is_string($functionsIterator->return))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].return : string expected");
							} else
								throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].return : mandatory param");
							
							if (property_exists($functionsIterator, "ref")) {
								if (!is_bool($functionsIterator->ref))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].ref : bool expected");
							} else
								$data->packages[$packagesCount]->functions[$functionsCount]->ref = false;
							
							if (property_exists($functionsIterator, "pointer")) {
								if (!is_bool($functionsIterator->pointer))
									throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].pointer : bool expected");
							}
							
							if (property_exists($functionsIterator, "args")) {
								if (!is_array($functionsIterator->args) && $functionsIterator->args != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args : array or null expected");
								
								$argsCount = 0;
								foreach ($functionsIterator->args as $argsIterator) {
									if (!is_object($argsIterator))
										throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "] : object expected");
									else {
										if (property_exists($argsIterator, "name")) {
											if (!is_string($argsIterator->name))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].name : string expected");
										} else
											throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].name : mandatory param");
										
										if (property_exists($argsIterator, "type")) {
											if (!is_string($argsIterator->type))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].type : string expected");
										} else
											throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].type : mandatory param");
										
										if (property_exists($argsIterator, "const")) {
											if (!is_bool($argsIterator->const))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].const : bool expected");
										} else
											$data->packages[$packagesCount]->functions[$functionsCount]->args[$argsCount]->const = false;
										
										if (property_exists($argsIterator, "ref")) {
											if (!is_bool($argsIterator->ref))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].ref : bool expected");
										} else
											$data->packages[$packagesCount]->functions[$functionsCount]->args[$argsCount]->ref = false;
										
										if (property_exists($argsIterator, "pointer")) {
											if (!is_bool($argsIterator->pointer))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].pointer : bool expected");
										} else
											$data->packages[$packagesCount]->functions[$functionsCount]->args[$argsCount]->pointer = false;
										
										if (property_exists($argsIterator, "default")) {
											if (!is_string($argsIterator->default) && $argsIterator->default != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].default : string or null expected");
										} else
											$data->packages[$packagesCount]->functions[$functionsCount]->args[$argsCount]->default = NULL;
										
										if (property_exists($argsIterator, "description")) {
											if (!is_string($argsIterator->description) && $argsIterator->description != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].args[" . $argsCount . "].description : string or null expected");
										} else
											$data->packages[$packagesCount]->functions[$functionsCount]->args[$argsCount]->description = NULL;
									}
									
									$argsCount++;
								}
							} else
								$data->packages[$packagesCount]->functions[$functionsCount]->args = NULL;
							
							if (property_exists($functionsIterator, "shortDescription")) {
								if (!is_string($functionsIterator->shortDescription) && $functionsIterator->shortDescription != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].shortDescription : string or null expected");
							} else
								$data->packages[$packagesCount]->functions[$functionsCount]->shortDescription = NULL;
							
							if (property_exists($functionsIterator, "description")) {
								if (!is_string($functionsIterator->description) && $functionsIterator->description != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].description : string or null expected");
							} else
								$data->packages[$packagesCount]->functions[$functionsCount]->description = NULL;
							
							if (property_exists($functionsIterator, "since")) {
								if (!is_string($functionsIterator->since) && $functionsIterator->since != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].since : string or null expected");
							} else
								$data->packages[$packagesCount]->functions[$functionsCount]->since = NULL;
							
							if (property_exists($functionsIterator, "deprecated")) {
								if (!is_bool($functionsIterator->deprecated))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].deprecated : bool expected");
							} else
								$data->packages[$packagesCount]->functions[$functionsCount]->deprecated = false;
							
							if (property_exists($functionsIterator, "author")) {
								if (!is_string($functionsIterator->author) && $functionsIterator->author != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].author : string or null expected");
							} else
								$data->packages[$packagesCount]->functions[$functionsCount]->author = NULL;
							
							if (property_exists($functionsIterator, "annotations")) {
								if (!is_array($functionsIterator->annotations) && $functionsIterator->annotations != NULL)
									throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].annotations : array or null expected");
								
								$annotationsCount = 0;
								foreach ($functionsIterator->annotations as $annotationsIterator) {
									if (!is_string($annotationsIterator) && !is_array($annotationsIterator))
										throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].annotations[$annotationsCount] : string or array expected");
									
									if (is_array($annotationsIterator)) {
										$annotationsCountCount = 0;
										foreach ($annotationsIterator as $annotationsIteratorIterator) {
											if (!is_string($annotationsIteratorIterator))
												throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].annotations[$annotationsCount][$annotationsCountCount] : string expected");
											
											$annotationsCountCount++;
										}
									}
									
									$annotationsCount++;
								}
							}
							
							if (property_exists($functionsIterator, "throws")) {
								if (!is_array($functionsIterator->throws) && $functionsIterator->throws != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].throws : array or null expected");
								
								$throwsCount = 0;
								foreach ($functionsIterator->throws as $throwsIterator) {
									if (!is_object($throwsIterator))
										throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].throws[$throwsCount] : object expected");
									
									if (property_exists($throwsIterator, "name")) {
										if (!is_string($throwsIterator->name))
											throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].throws[$throwsCount].name : string expected");
									}
									
									if (property_exists($throwsIterator, "description")) {
										if (!is_string($throwsIterator->description) && $throwsIterator->description != NULL)
											throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].functions[" . $functionsCount . "].throws[$throwsCount].description : string or null expected");
									}
									
									$throwsCount++;
								}
							}
							
							if (property_exists($functionsIterator, "template")) {
								if (!is_bool($functionsIterator->template))
									throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].template : bool expected");
							}
							
							if (property_exists($functionsIterator, "template_args")) {
								if (!is_array($functionsIterator->template_args) && $functionsIterator->template_args != NULL)
									throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].template_args : array or null expected");
								
								$argsCount = 0;
								foreach ($functionsIterator->template_args as $argsIterator) {
									if (!is_object($argsIterator))
										throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].template_args[$argsCount] : object expected");
									
									if (property_exists($argsIterator, "name")) {
										if (!is_string($argsIterator->name))
											throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].template_args[$argsCount].name : string expected");
									} else
										throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].template_args[$argsCount].name : mandatory param");
									
									if (property_exists($argsIterator, "description")) {
										if (!is_string($argsIterator->description) && $argsIterator->description != NULL)
											throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].template_args[$argsCount].description : string or null expected");
									}
									
									if (property_exists($argsIterator, "default")) {
										if (!is_string($argsIterator->default) && $argsIterator->default != NULL)
											throw new MalformedDataException("ROOT.packages[$packagesCount].functions[$functionsCount].template_args[$argsCount].default : string or null expected");
									}
									
									$argsCount++;
								}
							}
						}
						
						$functionsCount++;
					}
				} else
					$data->packages[$packagesCount]->functions = NULL;
				
				// LES CLASSES DANS LES PACKAGES
				if (property_exists($packagesIterator, "classes")) {
					if (!is_array($packagesIterator->classes) && $packagesIterator->classes != NULL)
						throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes : array or null expected");
					
					$classesCount = 0;
					foreach ($packagesIterator->classes as $classesIterator) {
						if (!is_object($classesIterator))
							throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "] : object expected");
						else {
							if (property_exists($classesIterator, "name")) {
								if (!is_string($classesIterator->name))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].name : string expected");
							} else
								throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].name : mandatory param");
							
							if (property_exists($classesIterator, "abstract")) {
								if (!is_bool($classesIterator->abstract))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].abstract : bool expected");
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->abstract = false;
							
							if (property_exists($classesIterator, "final")) {
								if (!is_bool($classesIterator->final))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].final : bool expected");
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->final = false;
							
							if (property_exists($classesIterator, "extends")) {
								if (!is_array($classesIterator->extends) && $classesIterator->extends != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].extends : array expected");
								
								$extendsCount = 0;
								foreach ($classesIterator->extends as $extendsIterator) {
									if (!is_string($extendsIterator))
										throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].extends[" . $extendsCount . "] : string expected");
									
									$extendsCount++;
								}
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->extends = NULL;
							
							if (property_exists($classesIterator, "implements")) {
								if (!is_array($classesIterator->implements) && $classesIterator->implements != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].implements : array expected");
								
								$implementsCount = 0;
								foreach ($classesIterator->implements as $implementsIterator) {
									if (!is_string($implementsIterator))
										throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].implements[" . $implementsCount . "] : string expected");
									
									$implementsCount++;
								}
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->implements = NULL;
							
							// MEMBRES DES CLASSES DES PACKAGES
							if (property_exists($classesIterator, "members")) {
								if (!is_array($classesIterator->members) && $classesIterator->members != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members : array or null expected");
								
								$membersCount = 0;
								foreach ($classesIterator->members as $membersIterator) {
									if (!is_object($membersIterator))
										throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "] : object expected");
									else {
										if (property_exists($membersIterator, "name")) {
											if (!is_string($membersIterator->name))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].name : string expected");
										} else
											throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].name : mandatory param");
										
										if (property_exists($membersIterator, "type")) {
											if (!is_string($membersIterator->type))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].type : string expected");
										} else
											throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].type : mandatory param");
										
										if (property_exists($membersIterator, "scope")) {
											if (!is_string($membersIterator->scope))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].scope : string expected");
											elseif ($membersIterator->scope != "public" &&
											        $membersIterator->scope != "private" &&
											        $membersIterator->scope != "protected")
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].scope : the only authorized values are \"public\", \"private\" or \"protected\"");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->scope = "public";
										
										if (property_exists($membersIterator, "const")) {
											if (!is_bool($membersIterator->const))
												throw new MalformedDataException("ROOT.pàackages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].const : bool expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->const = false;
										
										if (property_exists($membersIterator, "static")) {
											if (!is_bool($membersIterator->static))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].static : bool expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->static = false;
										
										if (property_exists($membersIterator, "pointer")) {
											if (!is_bool($membersIterator->pointer))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].pointer : bool expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->pointer = false;
										
										if (property_exists($membersIterator, "final")) {
											if (!is_bool($membersIterator->final))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].final : bool expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->final = false;
										
										if (property_exists($membersIterator, "description")) {
											if (!is_string($membersIterator->description) && $membersIterator->description != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].description : string or null expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->description = NULL;
										
										if (property_exists($membersIterator, "since")) {
											if (!is_string($membersIterator->since) && $membersIterator->since != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].since : string or null expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->since = NULL;
										
										if (property_exists($membersIterator, "deprecated")) {
											if (!is_bool($membersIterator->deprecated))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].deprecated : bool expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->deprecated = false;
										
										if (property_exists($membersIterator, "author")) {
											if (!is_string($membersIterator->author) && $membersIterator->author != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].members[" . $membersCount . "].author : string or null expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->members[$membersCount]->author = NULL;
										
										if (property_exists($membersIterator, "annotations")) {
											if (!is_array($membersIterator->annotations) && $membersIterator->annotations != NULL)
												throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].members[$membersCount].annotations : array or null expected");
											
											$annotationsCount = 0;
											foreach ($membersIterator->annotations as $annotationsIterator) {
												if (!is_string($annotationsIterator) && !is_array($annotationsIterator))
													throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].members[$membersCount].annotations[$annotationsCount] : string or array expected");
												
												if (is_array($annotationsIterator)) {
													$annotationsCountCount = 0;
													foreach ($annotationsIterator as $annotationsIteratorIterator) {
														if (!is_string($annotationsIteratorIterator))
															throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].members[$membersCount].annotations[$annotationsCount][$annotationsCountCount] : string expected");
														
														$annotationsCountCount++;
													}
												}
												
												$annotationsCount++;
											}
										}
									}
									
									$membersCount++;
								}
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->args = NULL;
							
							// METHODES DES CLASSES DANS LES PACKAGES
							if (property_exists($classesIterator, "methods")) {
								if (!is_array($classesIterator->methods) && $classesIterator->methods != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods : array or null expected");
								
								$methodsCount = 0;
								foreach ($classesIterator->methods as $methodsIterator) {
									if (!is_object($methodsIterator))
										throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "] : object expected");
									else {
										if (property_exists($methodsIterator, "name")) {
											if (!is_string($methodsIterator->name))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].name : string expected");
										} else
											throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].name : mandatory param");
										
										if (property_exists($methodsIterator, "return")) {
											if (!is_string($methodsIterator->return))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].return : string expected");
										} elseif (!property_exists($methodsIterator, "constructor") && !property_exists($methodsIterator, "destructor"))
											throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].return : mandatory param");
										
										if (property_exists($methodsIterator, "ref")) {
											if (!is_bool($methodsIterator->ref))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].ref : bool expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->ref = false;
										
										if (property_exists($methodsIterator, "pointer")) {
											if (!is_bool($methodsIterator->pointer))
												throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].methods[$methodsCount].pointer : bool expected");
										}
										
										// ARGUMENTS DES METHODES DES CLASSES DANS LES PACKAGES
										if (property_exists($methodsIterator, "args")) {
											if (!is_array($methodsIterator->args) && $methodsIterator->args != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].args : array or null expected");
											
											$argsCount = 0;
											foreach ($methodsIterator->args as $argsIterator) {
												if (!is_object($argsIterator))
													throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].args[" . $argsCount . "] : object expected");
												
												if (property_exists($argsIterator, "name")) {
													if (!is_string($argsIterator->name))
														throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].args[" . $argsCount . "].name : string expected");
												} else
													throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].args[" . $argsCount . "].name : mandatory param");
												
												if (property_exists($argsIterator, "type")) {
													if (!is_string($argsIterator->type))
														throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].args[" . $argsCount . "].type : string expected");
												} else
													throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].args[" . $argsCount . "].type : mandatory param");
												
												if (property_exists($argsIterator, "const")) {
													if (!is_bool($argsIterator->const))
														throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].args[" . $argsCount . "].const : bool expected");
												} else
													$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->args[$argsCount]->const = false;
												
												if (property_exists($argsIterator, "ref")) {
													if (!is_bool($argsIterator->ref))
														throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].args[" . $argsCount . "].ref : bool expected");
												} else
													$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->args[$argsCount]->ref = false;
												
												$argsCount++;
											}
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->args = NULL;
										
										if (property_exists($methodsIterator, "shortDescription")) {
											if (!is_string($methodsIterator->shortDescription) && $methodsIterator->shortDescription != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].shortDescription : string or null expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->shortDescription = NULL;
										
										if (property_exists($methodsIterator, "description")) {
											if (!is_string($methodsIterator->description) && $methodsIterator->description != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].description : string or null expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->description = NULL;
										
										if (property_exists($methodsIterator, "since")) {
											if (!is_string($methodsIterator->since) && $methodsIterator->since != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].since : string or null expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->since = NULL;
										
										if (property_exists($methodsIterator, "deprecated")) {
											if (!is_bool($methodsIterator->deprecated))
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].deprecated : bool expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->deprecated = false;
										
										if (property_exists($methodsIterator, "author")) {
											if (!is_string($methodsIterator->author) && $methodsIterator->author != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].author : string or null expected");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->author = NULL;
										
										if (property_exists($methodsIterator, "scope")) {
											if (!is_string($methodsIterator->scope) && $methodsIterator->scope != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].scope : string or null expected");
											elseif ($methodsIterator->scope != "public" &&
											        $methodsIterator->scope != "private" &&
											        $methodsIterator->scope != "protected")
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].methods[" . $methodsCount . "].scope : the only authorized values are \"public\", \"private\" or \"protected\"");
										} else
											$data->packages[$packagesCount]->classes[$classesCount]->methods[$methodsCount]->scope = "public";
										
										if (property_exists($methodsIterator, "annotations")) {
											if (!is_array($methodsIterator->annotations) && $methodsIterator->annotations != NULL)
												throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].methods[$methodsCount].annotations : array or null expected");
											
											$annotationsCount = 0;
											foreach ($methodsIterator->annotations as $annotationsIterator) {
												if (!is_string($annotationsIterator) && !is_array($annotationsIterator))
													throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].methods[$methodsCount].annotations[$annotationsCount] : string or array expected");
												
												if (is_array($annotationsIterator)) {
													$annotationsCountCount = 0;
													foreach ($annotationsIterator as $annotationsIteratorIterator) {
														if (!is_string($annotationsIteratorIterator))
															throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].methods[$methodsCount].annotations[$annotationsCount][$annotationsCountCount] : string expected");
														
														$annotationsCountCount++;
													}
												}
												
												$annotationsCount++;
											}
										}
										
										if (property_exists($methodsIterator, "constructor")) {
											if (!is_bool($methodsIterator->constructor))
												throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].methods[$methodsCount].constructor : bool expected");
										}
										
										if (property_exists($methodsIterator, "destructor")) {
											if (!is_bool($methodsIterator->destructor))
												throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].methods[$methodsCount].destructor : bool expected");
										}
										
										if (property_exists($methodsIterator, "throws")) {
											if (!is_array($methodsIterator->throws) && $methodsIterator->throws != NULL)
												throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].throws : array or null expected");
											
											$throwsCount = 0;
											foreach ($methodsIterator->throws as $throwsIterator) {
												if (!is_object($throwsIterator))
													throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].throws[$throwsCount] : object expected");
												
												if (property_exists($throwsIterator, "name")) {
													if (!is_string($throwsIterator->name))
														throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].throws[$throwsCount].name : string expected");
												}
												
												if (property_exists($throwsIterator, "description")) {
													if (!is_string($throwsIterator->description) && $throwsIterator->description != NULL)
														throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].throws[$throwsCount].description : string or null expected");
												}
												
												$throwsCount++;
											}
										}
										
										if (property_exists($methodsIterator, "abstract")) {
											if (!is_bool($methodsIterator->abstract))
												throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].methods[$methodsCount].abstract : bool expected");
										}
										
										if (property_exists($methodsIterator, "static")) {
											if (!is_bool($methodsIterator->static))
												throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].methods[$methodsCount].static : bool expected");
										}
									}
									
									$methodsCount++;
								}
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->methods = NULL;
							
							if (property_exists($classesIterator, "shortDescription")) {
								if (!is_string($classesIterator->shortDescription) && $classesIterator->shortDescription != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].shortDescription : string or null expected");
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->shortDescription = NULL;
							
							if (property_exists($classesIterator, "description")) {
								if (!is_string($classesIterator->description) && $classesIterator->description != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].description : string or null expected");
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->description = NULL;
							
							if (property_exists($classesIterator, "since")) {
								if (!is_string($classesIterator->since) && $classesIterator->since != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].since : string or null expected");
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->since = NULL;
							
							if (property_exists($classesIterator, "deprecated")) {
								if (!is_bool($classesIterator->deprecated))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].deprecated : bool expected");
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->deprecated = false;
							
							if (property_exists($classesIterator, "author")) {
								if (!is_string($classesIterator->author) && $classesIterator->author != NULL)
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].classes[" . $classesCount . "].author : string or null expected");
							} else
								$data->packages[$packagesCount]->classes[$classesCount]->author = NULL;
							
							if (property_exists($classesIterator, "annotations")) {
								if (!is_array($classesIterator->annotations) && $classesIterator->annotations != NULL)
									throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].annotations : array or null expected");
								
								$annotationsCount = 0;
								foreach ($classesIterator->annotations as $annotationsIterator) {
									if (!is_string($annotationsIterator) && !is_array($annotationsIterator))
										throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].annotations[$annotationsCount] : string or array expected");
									
									if (is_array($annotationsIterator)) {
										$annotationsCountCount = 0;
										foreach ($annotationsIterator as $annotationsIteratorIterator) {
											if (!is_string($annotationsIteratorIterator))
												throw new MalformedDataException("ROOT.packages[$packagesCount].classes[$classesCount].annotations[$annotationsCount][$annotationsCountCount] : string expected");
											
											$annotationsCountCount++;
										}
									}
									
									$annotationsCount++;
								}
							}
						}
						
						$classesCount++;
					}
				} else
					$data->packages[$packagesCount]->classes = NULL;
				
				// LES INTERFACES DANS LES PACKAGES
				if (property_exists($packagesIterator, "interfaces")) {
					if (!is_array($packagesIterator->interfaces) && $packagesIterator->interfaces != NULL)
						throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces : array or null expected");
					
					$interfacesCount = 0;
					foreach ($packagesIterator->interfaces as $interfacesIterator) {
						if (!is_object($interfacesIterator))
							throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "] : object expected");
						
						if (property_exists($interfacesIterator, "name")) {
							if (!is_string($interfacesIterator->name))
								throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "].name : string expected");
						} else
							throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "].name : mandatory param");
						
						if (property_exists($interfacesIterator, "shortDescription")) {
							if (!is_string($interfacesIterator->shortDescription) && $interfacesIterator->shortDescription != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].shortDescription : string or null expected");
						} else
							$data->packages[$packagesCount]->interfaces[$interfacesCount]->shortDescription = NULL;
						
						if (property_exists($interfacesIterator, "description")) {
							if (!is_string($interfacesIterator->description) && $interfacesIterator->description != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].description : string or null expected");
						} else
							$data->packages[$packagesCount]->interfaces[$interfacesCount]->description = NULL;
						
						if (property_exists($interfacesIterator, "author")) {
							if (!is_string($interfacesIterator->author) && $interfacesIterator->author != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].author : string or null expected");
						} else
							$data->packages[$packagesCount]->interfaces[$interfacesCount]->author = NULL;
						
						if (property_exists($interfacesIterator, "since")) {
							if (!is_string($interfacesIterator->since) && $interfacesIterator->since != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].since : string or null expected");
						} else
							$data->packages[$packagesCount]->interfaces[$interfacesCount]->since = NULL;
						
						if (property_exists($interfacesIterator, "deprecated")) {
							if (!is_bool($interfacesIterator->deprecated))
								throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].deprecated : bool expected");
						} else
							$data->packages[$packagesCount]->interfaces[$interfacesCount]->deprecated = false;
						
						// LES METHODES DANS LES INTERFACES DANS LES PACKAGES
						if (property_exists($interfacesIterator, "methods")) {
							if (!is_array($interfacesIterator->methods) && $interfacesIterator->methods != NULL)
								throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "].methods : array or null expected");
							
							$methodsCount = 0;
							foreach ($interfacesIterator->methods as $methodsIterator) {
								if (!is_object($methodsIterator))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "].methods[" . $methodsCount . "] : object expected");
								
								if (property_exists($methodsIterator, "name")) {
									if (!is_string($methodsIterator->name))
										throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "].methods[" . $methodsCount . "].name : string expected");
								} else
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "].methods[" . $methodsCount . "].name : mandatory param");
								
								if (property_exists($methodsIterator, "return")) {
									if (!is_string($methodsIterator->return))
										throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "].methods[" . $methodsCount . "].return : string expected");
								} elseif (!property_exists($methodsIterator, "constructor") && !property_exists($methodsIterator, "destructor"))
									throw new MalformedDataException("ROOT.packages[" . $packagesCount . "].interfaces[" . $interfacesCount . "].methods[" . $methodsCount . "].return : mandatory param");
								
								if (property_exists($methodsIterator, "ref")) {
									if (!is_bool($methodsIterator->ref))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].ref : bool expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->ref = false;
								
								if (property_exists($methodsIterator, "pointer")) {
									if (!is_bool($methodsIterator->pointer))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].pointer : bool expected");
								}
								
								// LES ARGUMENTS DES METHODES DANS LES INTERFACES DANS LES PACKAGES
								if (property_exists($methodsIterator, "args")) {
									if (!is_array($methodsIterator->args) && $methodsIterator->args != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args : array or null expected");
									
									$argsCount = 0;
									foreach ($methodsIterator->args as $argsIterator) {
										if (!is_object($argsIterator))
											throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount] : object expected");
										
										if (property_exists($argsIterator, "name")) {
											if (!is_string($argsIterator->name))
												throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].name : string expected");
										} else
											throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].name : mandatory param");
										
										if (property_exists($argsIterator, "type")) {
											if (!is_string($argsIterator->type))
												throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].type : string expected");
										} else
											throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].type : mandatory param");
										
										if (property_exists($argsIterator, "const")) {
											if (!is_bool($argsIterator->const))
												throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].const : bool expected");
										} else
											$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->args[$argsCount]->const = false;
										
										if (property_exists($argsIterator, "ref")) {
											if (!is_bool($argsIterator->ref))
												throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].ref : bool expected");
										} else
											$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->args[$argsCount]->ref = false;
										
										if (property_exists($argsIterator, "pointer")) {
											if (!is_bool($argsIterator->pointer))
												throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].pointer : bool expected");
										} else
											$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->args[$argsCount]->pointer = false;
										
										if (property_exists($argsIterator, "default")) {
											if (!is_string($argsIterator->default) && $argsIterator->default != NULL)
												throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].default : string or null expected");
										} else
											$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->args[$argsCount]->default = NULL;
										
										if (property_exists($argsIterator, "description")) {
											if (!is_string($argsIterator->description) && $argsIterator->description != NULL)
												throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].args[$argsCount].description : string or null expected");
										} else
											$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->args[$argsCount]->description = NULL;
										
										$argsCount++;
									}
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->args = NULL;
								
								if (property_exists($methodsIterator, "shortDescription")) {
									if (!is_string($methodsIterator->shortDescription) && $methodsIterator->shortDescription != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].shortDescription : string or null expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->shortDescription = NULL;
								
								if (property_exists($methodsIterator, "description")) {
									if (!is_string($methodsIterator->description) && $methodsIterator->description != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].description : string or null expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->description = NULL;
								
								if (property_exists($methodsIterator, "since")) {
									if (!is_string($methodsIterator->since) && $methodsIterator->since != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].since : string or null expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->since = NULL;
								
								if (property_exists($methodsIterator, "deprecated")) {
									if (!is_bool($methodsIterator->deprecated))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].deprecated : bool expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->deprecated = false;
								
								if (property_exists($methodsIterator, "author")) {
									if (!is_string($methodsIterator->author) && $methodsIterator->author != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].author : string or null expected");
								}
								
								if (property_exists($methodsIterator, "scope")) {
									if (!is_string($methodsIterator->scope) && $methodsIterator->scope != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].scope : string or null expected");
									elseif ($methodsIterator->scope != "public" &&
									        $methodsIterator->scope != "private" &&
									        $methodsIterator->scope != "protected")
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].scope : the only authorized values are \"public\", \"private\" or \"protected\"");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods[$methodsCount]->scope = "public";
								
								if (property_exists($methodsIterator, "annotations")) {
									if (!is_array($methodsIterator->annotations) && $methodsIterator->annotations != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].annotations : array or null expected");
								
									$annotationsCount = 0;
									foreach ($methodsIterator->annotations as $annotationsIterator) {
										if (!is_string($annotationsIterator) && !is_array($annotationsIterator))
											throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].annotations[$annotationsCount] : string or array expected");
										
										if (is_array($annotationsIterator)) {
											$annotationsCountCount = 0;
											foreach ($annotationsIterator as $annotationsIteratorIterator) {
												if (!is_string($annotationsIteratorIterator))
													throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].annotations[$annotationsCount][$annotationsCountCount] : string expected");
												
												$annotationsCountCount++;
											}
										}
										
										$annotationsCount++;
									}
								}
								
								if (property_exists($methodsIterator, "constructor")) {
									if (!is_bool($methodsIterator->constructor))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].constructor : bool expected");
								}
								
								if (property_exists($methodsIterator, "destructor")) {
									if (!is_bool($methodsIterator->destructor))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].destructor : bool expected");
								}
								
								if (property_exists($methodsIterator, "static")) {
									if (!is_bool($methodsIterator->static))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].static : bool expected");
								}
								
								if (property_exists($methodsIterator, "abstract")) {
									if (!is_bool($methodsIterator->abstract))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].methods[$methodsCount].abstract : bool expected");
								}
								
								$methodsCount++;
							}
						} else
							$data->packages[$packagesCount]->interfaces[$interfacesCount]->methods = NULL;
						
						// LES MEMBRES DES INTERFACES DANS LES PACKAGES
						if (property_exists($interfacesIterator, "members")) {
							if (!is_array($interfacesIterator->members) && $interfacesIterator->members != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members : array or null expected");
							
							$membersCount = 0;
							foreach ($interfacesIterator->members as $membersIterator) {
								if (!is_object($membersIterator))
									throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount] : object expected");
								
								if (property_exists($membersIterator, "name")) {
									if (!is_string($membersIterator->name))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].name : string expected");
								} else
									throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].name : mandatory param");
								
								if (property_exists($membersIterator, "scope")) {
									if (!is_string($membersIterator->scope) && $membersIterator->scope != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].scope : string or null expected");
									elseif ($membersIterator->scope != "public" &&
									        $membersIterator->scope != "private" &&
									        $membersIterator->scope != "protected")
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].scope : the only authorized values are \"public\", \"private\" or \"protected\"");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->scope = "public";
								
								if (property_exists($membersIterator, "type")) {
									if (!is_string($membersIterator->type))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].type : string expected");
								} else
									throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].type : mandatory param");
								
								if (property_exists($membersIterator, "const")) {
									if (!is_bool($membersIterator->const))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].const : bool expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->const = false;
								
								if (property_exists($membersIterator, "static")) {
									if (!is_bool($membersIterator->static))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].static : bool expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->static = false;
								
								if (property_exists($membersIterator, "pointer")) {
									if (!is_bool($membersIterator->pointer))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].pointer : bool expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->pointer = false;
								
								if (property_exists($membersIterator, "final")) {
									if (!is_bool($membersIterator->final))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].final : bool expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->final = false;
								
								if (property_exists($membersIterator, "description")) {
									if (!is_string($membersIterator->description) && $membersIterator->description != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].description : string or null expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->description = NULL;
								
								if (property_exists($membersIterator, "since")) {
									if (!is_string($membersIterator->since) && $membersIterator->since != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].since : string or null expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->since = NULL;
								
								if (property_exists($membersIterator, "deprecated")) {
									if (!is_bool($membersIterator->deprecated))
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].deprecated : bool expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->deprecated = false;
								
								if (property_exists($membersIterator, "author")) {
									if (!is_string($membersIterator->author) && $membersIterator->author != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].author : string or null expected");
								} else
									$data->packages[$packagesCount]->interfaces[$interfacesCount]->members[$membersCount]->author = NULL;
								
								if (property_exists($membersIterator, "annotations")) {
									if (!is_array($membersIterator->annotations) && $membersIterator->annotations != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].annotations : array or null expected");
									
									$annotationsCount = 0;
									foreach ($membersIterator->annotations as $annotationsIterator) {
										if (!is_string($annotationsIterator) && !is_array($annotationsIterator))
											throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].annotations[$annotationsCount] : string or array expected");
										
										if (is_array($annotationsIterator)) {
											$annotationsCountCount = 0;
											foreach ($annotationsIterator as $annotationsIteratorIterator) {
												if (!is_string($annotationsIteratorIterator))
													throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].members[$membersCount].annotations[$annotationsCount][$annotationsCountCount] : string expected");
												
												$annotationsCountCount++;
											}
										}
										
										$annotationsCount++;
									}
								}
								
								$membersCount++;
							}
						} else
							$data->packages[$packagesCount]->interfaces[$interfacesCount]->members = NULL;
						
						if (property_exists($interfacesIterator, "annotations")) {
							if (!is_array($interfacesIterator->annotations) && $interfacesIterator->annotations != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].annotations : array or null expected");
							
							$annotationsCount = 0;
							foreach ($interfacesIterator->annotations as $annotationsIterator) {
								if (!is_string($annotationsIterator) && !is_array($annotationsIterator))
									throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].annotations[$annotationsCount] : string or array expected");
								
								if (is_array($annotationsIterator)) {
									$annotationsCountCount = 0;
									foreach ($annotationsIterator as $annotationsIteratorIterator) {
										if (!is_string($annotationsIteratorIterator))
											throw new MalformedDataException("ROOT.packages[$packagesCount].interfaces[$interfacesCount].annotations[$annotationsCount][$annotationsCountCount] : string expected");
										
										$annotationsCountCount++;
									}
								}
								
								$annotationsCount++;
							}
						}
						
						$interfacesCount++;
					}
				} else
					$data->packages[$packagesCount]->interfaces = NULL;
				
				// LES VARIABLES DANS LES PACKAGES
				if (property_exists($packagesIterator, "variables")) {
					if (!is_array($packagesIterator->variables) && $packagesIterator->variables != NULL)
						throw new MalformedDataException("ROOT.packages[$packagesCount].variables : array or null expected");
					
					$variablesCount = 0;
					foreach ($packagesIterator->variables as $variablesIterator) {
						if (!is_object($variablesIterator))
							throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount] : object expected");
						
						if (property_exists($variablesIterator, "name")) {
							if (!is_string($variablesIterator->name))
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].name : string expected");
						} else
							throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].name : mandatory param");
						
						if (property_exists($variablesIterator, "type")) {
							if (!is_string($variablesIterator->type))
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].type : string expected");
						} else
							throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].type : mandatory param");
						
						if (property_exists($variablesIterator, "pointer")) {
							if (!is_bool($variablesIterator->pointer))
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].pointer : bool expected");
						} else
							$data->packages[$packagesCount]->variables[$variablesCount]->pointer = false;
						
						if (property_exists($variablesIterator, "const")) {
							if (!is_bool($variablesIterator->const))
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].const : bool expected");
						} else
							$data->packages[$packagesCount]->variables[$variablesCount]->const = false;
						
						if (property_exists($variablesIterator, "description")) {
							if (!is_string($variablesIterator->description) && $variablesIterator->description != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].description : string or null expected");
						} else
							$data->packages[$packagesCount]->variables[$variablesCount]->description = NULL;
						
						if (property_exists($variablesIterator, "author")) {
							if (!is_string($variablesIterator->author) && $variablesIterator->author != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].author : string or null expected");
						} else
							$data->packages[$packagesCount]->variables[$variablesCount]->author = NULL;
						
						if (property_exists($variablesIterator, "since")) {
							if (!is_string($variablesIterator->since) && $variablesIterator->since != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].since : string or null expected");
						} else
							$data->packages[$packagesCount]->variables[$variablesCount]->since = NULL;
						
						if (property_exists($variablesIterator, "deprecated")) {
							if (!is_bool($variablesIterator->deprecated))
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].deprecated : bool expected");
						} else
							$data->packages[$packagesCount]->variables[$variablesCount]->deprecated = false;
						
						if (property_exists($variablesIterator, "annotations")) {
							if (!is_array($variablesIterator->annotations) && $variablesIterator->annotations != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].annotations : array or null expected");
							
							$annotationsCount = 0;
							foreach ($variablesIterator->annotations as $annotationsIterator) {
								if (!is_string($annotationsIterator) && !is_array($annotationsIterator))
									throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].annotations[$annotationsCount] : string or array expected");
								
								if (is_array($annotationsIterator)) {
									$annotationsCountCount = 0;
									foreach ($annotationsIterator as $annotationsIteratorIterator) {
										if (!is_string($annotationsIteratorIterator))
											throw new MalformedDataException("ROOT.packages[$packagesCount].variables[$variablesCount].annotations[$annotationsCount][$annotationsCountCount] : string expected");
										
										$annotationsCountCount++;
									}
								}
								
								$annotationsCount++;
							}
						}
						
						$variablesCount++;
					}
				} else
					$data->packages[$packagesCount]->variables = NULL;
				
				// LES ANNOTATIONS DANS LES PACKAGES
				if (property_exists($packagesIterator, "annotations")) {
					if (!is_array($packagesIterator->annotations) && $packagesIterator->annotations != NULL)
						throw new MalformedDataException("ROOT.packages[$packagesCount].annotations : array or null expected");
					
					$annotationsCount = 0;
					foreach ($packagesIterator->annotations as $annotationsIterator) {
						if (!is_object($annotationsIterator))
							throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount] : object expected");
						
						if (property_exists($annotationsIterator, "name")) {
							if (!is_string($annotationsIterator->name))
								throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].name : string or expected");
						} else
							throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].name : mandatory param");
						
						if (property_exists($annotationsIterator, "shortDescription")) {
							if (!is_string($annotationsIterator->shortDescription) && $annotationsIterator->shortDescription != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].shortDescription : string or null expected");
						} else
							$data->packages[$packagesCount]->annotations[$annotationsCount]->shortDescription = NULL;
						
						if (property_exists($annotationsIterator, "description")) {
							if (!is_string($annotationsIterator->description) && $annotationsIterator->description != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].description : string or null expected");
						} else
							$data->packages[$packagesCount]->annotations[$annotationsCount]->description = NULL;
						
						if (property_exists($annotationsIterator, "author")) {
							if (!is_string($annotationsIterator->author) && $annotationsIterator->author != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].author : string or null expected");
						} else
							$data->packages[$packagesCount]->annotations[$annotationsCount]->author = NULL;
						
						if (property_exists($annotationsIterator, "since")) {
							if (!is_string($annotationsIterator->since) && $annotationsIterator->since != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].since : string or null expected");
						} else
							$data->packages[$packagesCount]->annotations[$annotationsCount]->since = NULL;
						
						if (property_exists($annotationsIterator, "deprecated")) {
							if (!is_bool($annotationsIterator->deprecated))
								throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].deprecated : bool expected");
						} else
							$data->packages[$packagesCount]->annotations[$annotationsCount]->deprecated = false;
						
						// LES ARGUMENTS DES ANNOTATIONS DANS LES PACKAGES
						if (property_exists($annotationsIterator, "args")) {
							if (!is_array($annotationsIterator->args) && $annotationsIterator->args != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].args : array or null expected");
							
							$argsCount = 0;
							foreach ($annotationsIterator->args as $argsIterator) {
								if (!is_object($argsIterator))
									throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].args[$argsCount] : object expected");
								
								if (property_exists($argsIterator, "name")) {
									if (!is_string($argsIterator->name))
										throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].args[$argSCount].name : string expected");
								} else
									throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].args[$argsCount].name : mandatory param");
								
								if (property_exists($argsIterator, "type")) {
									if (!is_string($argsIterator->type))
										throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].args[$argSCount].type : string expected");
								} else
									throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].args[$argSCount].type : mandatory param");
								
								if (property_exists($argsIterator, "description")) {
									if (!is_string($argsIterator->description) && $argsIterator->description != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].args[$argsCount].description : string or null expected");
								} else
									$data->packages[$packagesCount]->annotations[$annotationsCount]->description = NULL;
								
								if (property_exists($argsIterator, "default")) {
									if (!is_string($argsIterator->default) && $argsIterator->default != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].annotations[$annotationsCount].args[$argsCount].default : string or null expected");
								}
								
								$argsCount++;
							}
						} else
							$data->packages[$packagesCount]->annotations[$annotationsCount]->args = NULL;
						
						$annotationsCount++;
					}
				}
				
				// LES CONSTANTES DE PREPROCESSEUR DANS LES PACKAGES
				if (property_exists($packagesIterator, "preprocessorConstants")) {
					if (!is_array($packagesIterator->preprocessorConstants) && $packagesIterator->preprocessorConstants != NULL)
						throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants : array or null expected");
					
					$preprocessorConstantsCount = 0;
					foreach ($packagesIterator->preprocessorConstants as $preprocessorConstantsIterator) {
						if (!is_object($preprocessorConstantsIterator))
							throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants[$preprocessorConstantsCount] : object expected");
						
						if (property_exists($preprocessorConstantsIterator, "name")) {
							if (!is_string($preprocessorConstantsIterator->name))
								throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants[$preprocessorConstantsCount].name : string expected");
						} else
							throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants[$preprocessorConstantsCount].name : mandatory param");
						
						if (property_exists($preprocessorConstantsIterator, "value")) {
							if (!is_string($preprocessorConstantsIterator->value) && $preprocessorConstantsIterator->value != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants[$preprocessorConstantsCount].value : string or null expected");
						} else
							$data->packages[$packagesCount]->preprocessorConstants[$preprocessorConstantsCount]->value = NULL;
						
						if (property_exists($preprocessorConstantsIterator, "description")) {
							if (!is_string($preprocessorConstantsIterator->description) && $preprocessorConstantsIterator->description != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants[$preprocessorConstantsCount].description : string or null expected");
						} else
							$data->packages[$packagesCount]->preprocessorConstants[$preprocessorConstantsCount]->description = NULL;
						
						if (property_exists($preprocessorConstantsIterator, "author")) {
							if (!is_string($preprocessorConstantsIterator->author) && $preprocessorConstantsIterator->author != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants[$preprocessorConstantsCount].author : string or null expected");
						} else
							$data->packages[$packagesCount]->preprocessorConstants[$preprocessorConstantsCount]->author = NULL;
						
						if (property_exists($preprocessorConstantsIterator, "since")) {
							if (!is_string($preprocessorConstantsIterator->since) && $preprocessorConstantsIterator->since != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants[$preprocessorConstantsCount].since : string or null expected");
						} else
							$data->packages[$packagesCount]->preprocessorConstants[$preprocessorConstantsCount]->since = NULL;
						
						if (property_exists($preprocessorConstantsIterator, "deprecated")) {
							if (!is_bool($preprocessorConstantsIterator->deprecated))
								throw new MalformedDataException("ROOT.packages[$packagesCount].preprocessorConstants[$preprocessorConstantsCount].deprecated : bool expected");
						} else
							$data->packages[$packagesCount]->preprocessorConstants[$preprocessorConstantsCount]->deprecated = false;
						
						$preprocessorConstantsCount++;
					}
				} else
					$data->packages[$packagesCount]->preprocessorConstants = NULL;
				
				// LES SUITCASES DANS LES PACKAGES
				if (property_exists($packagesIterator, "suitcases")) {
					if (!is_array($packagesIterator->suitcases) && $packagesIterator->suitcases != NULL)
						throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases : array or null expected");
					
					$suitcasesCount = 0;
					foreach ($packagesIterator->suitcases as $suitcasesIterator) {
						if (!is_object($suitcasesIterator))
							throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount] : object expected");
						
						if (property_exists($suitcasesIterator, "name")) {
							if (!is_string($suitcasesIterator->name))
								throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].name : string expected");
						} else
							throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].name : mandatory param");
						
						if (property_exists($suitcasesIterator, "shortDescription")) {
							if (!is_string($suitcasesIterator->shortDescription) && $suitcasesIterator->shortDescription != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].shortDescription : string or null expected");
						}
						
						if (property_exists($suitcasesIterator, "description")) {
							if (!is_string($suitcasesIterator->description) && $suitcasesIterator->description != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].description : string or null expected");
						}
						
						if (property_exists($suitcasesIterator, "author")) {
							if (!is_string($suitcasesIterator->author) && $suitcasesIterator->author != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].author : string or null expected");
						}
						
						if (property_exists($suitcasesIterator, "since")) {
							if (!is_string($suitcasesIterator->since) && $suitcasesIterator->since != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].since : string or null expected");
						}
						
						if (property_exists($suitcasesIterator, "deprecated")) {
							if (!is_bool($suitcasesIterator->deprecated))
								throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].deprecated : bool expected");
						}
						
						if (property_exists($suitcasesIterator, "content")) {
							if (!is_array($suitcasesIterator->content) && $suitcasesIterator->content != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].content : array or null expected");
							
							$contentCount = 0;
							foreach ($suitcasesIterator->content as $contentIterator) {
								if (!is_object($contentIterator))
									throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].content[$contentCount] : object expected");
								
								if (property_exists($contentIterator, "name")) {
									if (!is_string($contentIterator->name))
										throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].content[$contentCount].name : string expected");
								} else
									throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].content[$contentCount].name : mandatory param");
								
								if (property_exists($contentIterator, "value")) {
									if (!is_string($contentIterator->value) && $contentIterator->value != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].content[$contentCount].value : string or null expected");
								}
								
								if (property_exists($contentIterator, "description")) {
									if (!is_string($contentIterator->description) && $contentIterator->description != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].content[$contentCount].description : string or null expected");
								}
								
								if (property_exists($contentIterator, "since")) {
									if (!is_string($contentIterator->since) && $contentIterator->since != NULL)
										throw new MalformedDataException("ROOT.packages[$packagesCount].suitcases[$suitcasesCount].content[$contentCount].since : string or null expected");
								}
								
								$contentCount++;
							}
						}
						
						$suitcasesCount++;
					}
				}
				
				// LES TYPEDEFS DANS LES PACKAGES
				if (property_exists($packagesIterator, "typedefs")) {
					if (!is_array($packagesIterator->typedefs) && $packagesIterator->typedefs != NULL)
						throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs : array or null expected");
					
					$typedefsCount = 0;
					
					foreach ($packagesIterator->typedefs as $typedefsIterator) {
						if (!is_object($typedefsIterator))
							throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount] : object expected");
						
						if (property_exists($typedefsIterator, "name")) {
							if (!is_string($typedefsIterator->name))
								throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount].name : string expected");
						} else
							throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount].name : mandatory param");
						
						if (property_exists($typedefsIterator, "value")) {
							if (!is_string($typedefsIterator->value))
								throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount].value : string expected");
						} else
							throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount].value : mandatory param");
						
						if (property_exists($typedefsIterator, "description")) {
							if (!is_string($typedefsIterator->description) && $typedefsIterator->description != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount].description : string or null expected");
						}
						
						if (property_exists($typedefsIterator, "author")) {
							if (!is_string($typedefsIterator->author) && $typedefsIterator->author != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount].author : string or null expected");
						}
						
						if (property_exists($typedefsIterator, "since")) {
							if (!is_string($typedefsIterator->since) && $typedefsIterator->since != NULL)
								throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount].since : string or null expected");
						}
						
						if (property_exists($typedefsIterator, "deprecated")) {
							if (!is_bool($typedefsIterator->deprecated))
								throw new MalformedDataException("ROOT.packages[$packagesCount].typedefs[$typedefsCount].deprecated : bool expected");
						}
						
						$typedefsCount++;
					}
				}
			}
			
			$packagesCount++;
		}
	}
}


function table(array $datas, string $title = NULL)
{
	$retval = "<table class=\"table table-striped datatable\">";
	
	if ($title != NULL)
		$retval .= "<thead><tr><th colspan=\"2\"><div class=\"content\">$title</div></th></tr></thead>";
	
	foreach ($datas as $k => $v)
	{
		$retval .= "<tr><td>" . $k . "</td><td>" . $v . "</td></tr>";
	}
	
	$retval .= "</table>";
	
	return $retval;
}

function getHeaderMenu(bool $isElement = false)
{
	$elementPrefix = ($isElement) ? "../" : "";
	return "<nav class=\"navbar navbar-default\"><div class=\"container-fluid\"><div class=\"navbar-header\"><a class=\"navbar-brand\" href=\"" . $elementPrefix . "index.html\">Documentation</a></div><div class=\"collapse navbar-collapse\"><ul class=\"nav navbar-nav\"><li><a href=\"" . $elementPrefix . "packages.html\">Packages</a></li><li><a href=\"" . $elementPrefix . "deprecated.html\">Deprecated</a></li></ul></div></div></nav><br/>";
}

abstract class Lex
{
	public static $LANG = "fr";
	public static $GENRE_F = 'f';
	public static $GENRE_M = 'm';
	
	public static function getDeprecatedWarning()
	{
		switch (self::$LANG)
		{
			case "fr": return "<b>Obsolète</b> : Cet élément est obsolète.";break;
		}
	}
	
	public static function getStatisticsMessage()
	{
		switch (self::$LANG)
		{
			case "fr": return "%1\$d file(s) created.\n";break;
		}
	}
	
	public static function getVersionMin()
	{
		switch (self::$LANG)
		{
			case "fr": return "Version min.";break;
		}
	}
	
	public static function getInformations()
	{
		switch (self::$LANG)
		{
			case "fr": return "INFORMATIONS";break;
		}
	}
	
	public static function getVersion()
	{
		switch (self::$LANG)
		{
			case "fr": return "Version";break;
		}
	}
	
	public static function getFonctions()
	{
		switch (self::$LANG)
		{
			case "fr": return "FONCTIONS";break;
		}
	}
	
	public static function getClasses()
	{
		switch (self::$LANG)
		{
			case "fr": return "CLASSES";break;
		}
	}
	
	public static function getInterfaces()
	{
		switch (self::$LANG)
		{
			case "fr": return "INTERFACES";break;
		}
	}
	
	public static function getVariables()
	{
		switch (self::$LANG)
		{
			case "fr": return "VARIABLES";break;
		}
	}
	
	public static function getAnnotations(bool $majuscules = true)
	{
		switch (self::$LANG)
		{
			case "fr": return ($majuscules) ? "ANNOTATIONS" : "Annotations";break;
		}
	}
	
	public static function getConstantesDePreprocesseur()
	{
		switch (self::$LANG)
		{
			case "fr": return "CONSTANTES DE PREPROCESSEUR";break;
		}
	}
	
	public static function getValeurDeRetour()
	{
		switch (self::$LANG)
		{
			case "fr": return "Valeur de retour";break;
		}
	}
	
	public static function getReference()
	{
		switch (self::$LANG)
		{
			case "fr": return "Référence";break;
		}
	}
	
	public static function getOui()
	{
		switch (self::$LANG)
		{
			case "fr": return "Oui";break;
		}
	}
	
	public static function getNon()
	{
		switch (self::$LANG)
		{
			case "fr": return "Non";break;
		}
	}
	
	public static function getAuteur()
	{
		switch (self::$LANG)
		{
			case "fr": return "Auteur";break;
		}
	}
	
	public static function getArguments()
	{
		switch (self::$LANG)
		{
			case "fr": return "ARGUMENTS";break;
		}
	}
	
	public static function getAbstrait(string $genre)
	{
		switch (self::$LANG)
		{
			case "fr": return ($genre == self::$GENRE_F) ? "Abstraite" : "Abstrait";break;
		}
	}
	
	public static function getFinal(string $genre)
	{
		switch (self::$LANG)
		{
			case "fr": return ($genre == self::$GENRE_F) ? "Finale" : "Final";break;
		}
	}
	
	public static function getEtends()
	{
		switch (self::$LANG)
		{
			case "fr": return "Etends";break;
		}
	}
	
	public static function getImplemente()
	{
		switch (self::$LANG)
		{
			case "fr": return "Implémente";break;
		}
	}
	
	public static function getMembres()
	{
		switch (self::$LANG)
		{
			case "fr": return "MEMBRES";break;
		}
	}
	
	public static function getMethodes()
	{
		switch (self::$LANG)
		{
			case "fr": return "METHODES";break;
		}
	}
	
	public static function getRetourneUneReference()
	{
		switch (self::$LANG)
		{
			case "fr": return "Retourne une référence";break;
		}
	}
	
	public static function getPortee()
	{
		switch (self::$LANG)
		{
			case "fr": return "Portée";break;
		}
	}
	
	public static function getType()
	{
		switch (self::$LANG)
		{
			case "fr": return "Type";break;
		}
	}
	
	public static function getConstant(string $genre)
	{
		switch (self::$LANG)
		{
			case "fr": return ($genre == self::$GENRE_F) ? "Constante" : "Constant";break;
		}
	}
	
	public static function getPointeur()
	{
		switch (self::$LANG)
		{
			case "fr": return "Pointeur";break;
		}
	}
	
	public static function getValeur()
	{
		switch (self::$LANG)
		{
			case "fr": return "Valeur";break;
		}
	}
	
	public static function getAucun(string $genre)
	{
		switch (self::$LANG)
		{
			case "fr": return ($genre == self::$GENRE_F) ? "Aucune" : "Aucun";break;
		}
	}
	
	public static function getFonction()
	{
		switch (self::$LANG)
		{
			case "fr": return "Fonction";break;
		}
	}
	
	public static function getClass()
	{
		switch (self::$LANG)
		{
			case "fr": return "Classe";break;
		}
	}
	
	public static function getInterface()
	{
		switch (self::$LANG)
		{
			case "fr": return "Interface";break;
		}
	}
	
	public static function getVariable()
	{
		switch (self::$LANG)
		{
			case "fr": return "Variable";break;
		}
	}
	
	public static function getAnnotation()
	{
		switch (self::$LANG)
		{
			case "fr": return "Annotation";break;
		}
	}
	
	public static function getConstanteDePreprocesseur()
	{
		switch (self::$LANG)
		{
			case "fr": return "Constante de préprocesseur";break;
		}
	}
	
	public static function getPackage()
	{
		switch (self::$LANG)
		{
			case "fr": return "Package";break;
		}
	}
	
	public static function getMethode()
	{
		switch (self::$LANG)
		{
			case "fr": return "Méthode";break;
		}
	}
	
	public static function getConstructeurs()
	{
		switch (self::$LANG)
		{
			case "fr": return "CONSTRUCTEURS";break;
		}
	}
	
	public static function getConstructeur()
	{
		switch (self::$LANG)
		{
			case "fr": return "Constructeur";break;
		}
	}
	
	public static function getDestructeurs()
	{
		switch (self::$LANG)
		{
			case "fr": return "DESTRUCTEURS";break;
		}
	}
	
	public static function getDestructeur()
	{
		switch (self::$LANG)
		{
			case "fr": return "Destructeur";break;
		}
	}
	
	public static function getExceptions()
	{
		switch (self::$LANG)
		{
			case "fr": return "Exceptions";break;
		}
	}
	
	public static function getStatic()
	{
		switch (self::$LANG)
		{
			case "fr": return "Statique";break;
		}
	}
	
	public static function getSuitcases()
	{
		switch (self::$LANG)
		{
			case "fr": return "SUITCASES";break;
		}
	}
	
	public static function getSuitcase()
	{
		switch (self::$LANG)
		{
			case "fr": return "Suitcase";break;
		}
	}
	
	public static function getKeys()
	{
		switch (self::$LANG)
		{
			case "fr": return "CLEFS";break;
		}
	}
	
	public static function getRetourneUnPointeur()
	{
		switch (self::$LANG)
		{
			case "fr": return "Retourne un pointeur";break;
		}
	}
	
	public static function getTypedefs()
	{
		switch (self::$LANG)
		{
			case "fr": return "TYPEDEFS";break;
		}
	}
	
	public static function getTypedef()
	{
		switch (self::$LANG)
		{
			case "fr": return "Typedef";break;
		}
	}
	
	public static function getNew()
	{
		switch (self::$LANG)
		{
			case "fr": return "NEW";break;
		}
	}
	
	public static function getNewLabel()
	{
		return "<span class=\"label label-success\">" . self::getNew() . "</span>";
	}
	
	public static function getTemplate()
	{
		switch (self::$LANG)
		{
			case "fr": return "Template";break;
		}
	}
	
	public static function getTemplateArgs(bool $majuscules)
	{
		switch (self::$LANG)
		{
			case "fr": return ($majuscules) ? "ARGUMENTS DE TEMPLATE" : "Arguments de template";break;
		}
	}
}

/**
 * Le chercheur d'objets.
 * 
 * Le chercheur d'objets permet de détecter si un objet dont les données font référence est aussi présent dans les données.
 */
abstract class ObjectsFinder
{
	public static $TYPE_STD            = "std";
	public static $TYPE_THROW          = "throw";
	public static $TYPE_TYPE           = "type";
	public static $TYPE_EXTEND         = "extend";
	public static $TYPE_IMPLEMENTATION = "implementation";
	public static $TYPE_ANNOTATION     = "annotation";

	protected static $documentedFunctions             = [];
	protected static $documentedClasses               = [];
	protected static $documentedInterfaces            = [];
	protected static $documentedVariables             = [];
	protected static $documentedAnnotations           = [];
	protected static $documentedPreprocessorConstants = [];
	protected static $documentedPackages              = [];
	protected static $documentedEnums                 = [];
	protected static $documentedTypedefs              = [];

	/**
	 * Retourne si un objet est défini dans la documentation.
	 * 
	 * @param string obj
	 * L'objet à rechercher.
	 * @param string type
	 * Le type de recherche à effectuer. Utilisez ObjectsFinder::$TYPE_*
	 * 
	 * @return bool
	 */
	public static function objectExists(string $obj, string $type): bool
	{
		$retval = false;
		
		if (preg_match("#^.+\[\d*\]$#", $obj))
		{
			// si ça correspond au schéma des tableaux calypso
			$obj = preg_split("#[\[\]]#", $obj)[0];
		}
		
		switch ($type)
		{
			case self::$TYPE_STD:
				if (in_array($obj, self::$documentedClasses) ||
				    in_array($obj, self::$documentedEnums) ||
				    in_array($obj, self::$documentedInterfaces) ||
				    in_array($obj, self::$documentedTypedefs))
					$retval = true;
				break;
			
			case self::$TYPE_THROW:
				if (in_array($obj, self::$documentedClasses) ||
				    in_array($obj, self::$documentedTypedefs))
					$retval = true;
				break;
			
			case self::$TYPE_TYPE:
				if (in_array($obj, self::$documentedClasses) ||
				    in_array($obj, self::$documentedEnums) ||
				    in_array($obj, self::$documentedTypedefs))
					$retval = true;
				break;
			
			case self::$TYPE_EXTEND:
				if (in_array($obj, self::$documentedClasses) ||
				    in_array($obj, self::$documentedInterfaces) ||
				    in_array($obj, self::$documentedTypedefs))
					$retval = true;
				break;
			
			case self::$TYPE_IMPLEMENTATION:
				if (in_array($obj, self::$documentedInterfaces) ||
				    in_array($obj, self::$documentedTypedefs))
					$retval = true;
				break;
			
			case self::$TYPE_ANNOTATION:
				if (in_array($obj, self::$documentedAnnotations))
					$retval = true;
				break;
		}
		
		return $retval;
	}

	/**
	 * Enregistre une fonction dans la base de données des fonctions présentes dans la documentation.
	 * 
	 * @param string pkgName
	 * Le nom du package de la fonction. laissez vide pour signifier "pas de package".
	 * @param string name
	 * Le nom de la fonction.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerFunction(string $pkgName, string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #2 cannot be empty");

		$eax = "";

		if ($pkgName != "")
			$eax .= $pkgName . ".";
		$eax .= $name;
		self::$documentedFunctions[] = $eax;

		return;
	}

	/**
	 * Enregistre une classe dans la base de données des classes présentes dans la documentation.
	 * 
	 * @param string pkgName
	 * Le nom du package de la classe. Laissez vide pour signifier "pas de package".
	 * @param string name
	 * Le nom de la classe.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerClass(string $pkgName, string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #2 cannot be empty");
		
		$register = "";

		if ($pkgName != "")
			$register .= $pkgName . ".";
		$register .= $name;
		self::$documentedClasses[] = $register;

		return;
	}

	/**
	 * Enregistre un interface dans la base de données des interfaces présents dans la documentation.
	 * 
	 * @param string pkgName
	 * Le nom du package de l'interface. Laissez vide pour signifier "pas de package".
	 * @param string name
	 * Le nom de l'interface.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerInterface(string $pkgName, string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #2 cannot be empty");
		
		$reg = "";

		if ($pkgName != "")
			$reg .= $pkgName . ".";
		$reg .= $name;
		self::$documentedInterfaces[] = $reg;

		return;
	}

	/**
	 * Enregistre une variable dans la base de données des variables présentes dans la documentation.
	 * 
	 * @param string pkgName
	 * Le nom du package de la variable. Laissez vide pour signifier "pas de package".
	 * @param string name
	 * Le nom de la variable.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerVariable(string $pkgName, string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #2 cannot be empty");
		
		$reg = "";

		if ($pkgName != "")
			$reg .= $pkgName . ".";
		$reg .= $name;
		self::$documentedVariables[] = $reg;

		return;
	}

	/**
	 * Enregistre une annotation dans la base de données des annotations présentes dans la documentation.
	 * 
	 * Ne mettez pas le "@" devant le nom de l'annotation.
	 * 
	 * @param string pkgName
	 * Le nom du package de l'annotation. Laissez vide pour signifier "pas de package".
	 * @param string name
	 * Le nom de l'annotation.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerAnnotation(string $pkgName, string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #2 cannot be empty");
		
		$reg = "";

		if ($pkgName != "")
			$reg .= $pkgName . ".";
		$reg .= $name;
		self::$documentedAnnotations[] = $reg;

		return;
	}

	/**
	 * Enregistre une constante de préprocesseur dans la base de données des constantes de préprocesseur présentes dans la documentation.
	 * 
	 * @param string pkgName
	 * Le nom du package de la constante de préprocesseur. Laissez vide pour signifier "pas de package".
	 * @param string name
	 * Le nom de la constante de préprocesseur.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerPreprocessorConstant(string $pkgName, string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #2 cannot be empty");
		
		$reg = "";

		if ($pkgName != "")
			$reg .= $pkgName . ".";
		$reg .= $name;
		self::$documentedPreprocessorConstants[] = $reg;

		return;
	}

	/**
	 * Enregistre un package dans la base de données des packages présents dans la documentation.
	 * 
	 * @param string name
	 * Le nom du package.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerPackage(string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #1 cannot be empty");
		
		$reg = $name;
		self::$documentedPackages[] = $reg;

		return;
	}

	/**
	 * Enregistre une enum dans la base de données des enums présentes dans la documentation.
	 * 
	 * @param string pkgName
	 * Le nom du package de l'enum. Laissez vide pour signifier "pas de package".
	 * @param string name
	 * Le nom de l'enum.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerEnum(string $pkgName, string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #2 cannot be empty");
		
		$reg = "";

		if ($pkgName != "")
			$reg .= $pkgName . ".";
		$reg .= $name;
		self::$documentedEnums[] = $reg;

		return;
	}

	/**
	 * Enregistre un typedef dans la base de données des typedefs présents dans la documentation.
	 * 
	 * @param string pkgName
	 * Le nom du package du typedef. Laissez vide pour signifier "pas de package".
	 * @param string name
	 * Le nom du typedef.
	 * 
	 * @return void
	 * @throws IllegalBlankStringException
	 */
	public static function registerTypedef(string $pkgName, string $name): void
	{
		if ($name == "")
			throw new IllegalBlankStringException("Argument #2 cannot be empty");
		
		$reg = "";

		if ($pkgName != "")
			$reg .= $pkgName . ".";
		$reg .= $name;
		self::$documentedTypedefs[] = $reg;

		return;
	}

	/**
	 * Retourne le nombre de fonctions enregistrées.
	 * 
	 * @return int
	 */
	public static function getRegisteredFunctionsCount(): int
	{
		return sizeof(self::$documentedFunctions);
	}

	/**
	 * Retourne le nombre de classes enregistrées.
	 * 
	 * @return int
	 */
	public static function getRegisteredClassesCount(): int
	{
		return sizeof(self::$documentedClasses);
	}

	/**
	 * Retourne le nombre d'interfaces enregistrés.
	 * 
	 * @return int
	 */
	public static function getRegisteredInterfacesCount(): int
	{
		return sizeof(self::$documentedInterfaces);
	}

	/**
	 * Retourne le nombre de variables enregistrées.
	 * 
	 * @return int
	 */
	public static function getRegisteredVariablesCount(): int
	{
		return sizeof(self::$documentedVariables);
	}

	/**
	 * Retourne le nombre d'annotations enregistrées.
	 * 
	 * @return int
	 */
	public static function getRegisteredAnnotationsCount(): int
	{
		return sizeof(self::$documentedAnnotations);
	}

	/**
	 * Retourne le nombre de constantes de préprocesseur enregistrées.
	 * 
	 * @return int
	 */
	public static function getRegisteredPreprocessorConstantsCount(): int
	{
		return sizeof(self::$documentedPreprocessorConstants);
	}

	/**
	 * Retourne le nombre de packages enregistrés.
	 * 
	 * @return int
	 */
	public static function getRegisteredPackagesCount(): int
	{
		return sizeof(self::$documentedPackages);
	}

	/**
	 * Retourne le nombre d'enums enregistrées.
	 * 
	 * @return int
	 */
	public static function getRegisteredEnumsCount(): int
	{
		return sizeof(self::$documentedEnums);
	}

	/**
	 * Retourne le nombre de typedefs enregistrés.
	 * 
	 * @return int
	 */
	public static function getRegisteredTypedefsCount(): int
	{
		return sizeof(self::$documentedTypedefs);
	}
	
	/**
	 * Retourne un string contenant le code HTML à mettre pour afficher l'objet (si l'objet existe,
	 * ce qu'il retourne contient un hyperlien...)
	 * 
	 * @param string obj
	 * Le nom de l'objet.
	 * 
	 * @param string type
	 * Le type d'objet. Utilisez ObjectsFinder::$TYPE_*
	 * 
	 * @param bool usingCodeMarkup
	 * Si TRUE, place le code HTML entre des balises code
	 * 
	 * @return string
	 */
	public static function renderObject(string $obj, string $type, bool $usingCodeMarkup = true): string
	{
		$array   = false;
		$objName = $obj;
		$objSize = "";
		$retval  = ($usingCodeMarkup) ? "<code>" : "";
		$objDisplayedName = "";
		
		if (preg_match("#^.+\[\d*\]$#", $obj)) {
			// si ça correspond au schéma des tableaux calypso
			$array = true;
			$preg = preg_split("#[\[\]]#", $obj);
			$objName = $preg[0];
			$objSize = (sizeof($preg) >= 2) ? $preg[1] : "";
		}
		
		if ($type == self::$TYPE_ANNOTATION)
			$objDisplayedName .= "@";
		$objDisplayedName .= $objName;
		
		if (!self::objectExists($objName, $type)) {
			if ($array)
				$retval .= $objDisplayedName . "[$objSize]";
			else
				$retval .= "$objDisplayedName";
		} else {
			if ($array)
				$retval .= "<a href=\"$objName.html\">$objDisplayedName</a>[$objSize]";
			else
				$retval .= "<a href=\"$objName.html\">$objDisplayedName</a>";
		}
		
		$retval .= ($usingCodeMarkup) ? "</code>" : "";
		
		return $retval;
	}
}

if ($_SERVER['argv'][1] == NULL ||
    $_SERVER['argv'][1] == "/?")
{
	help();
	exit();
}
$input = $_SERVER['argv'][1];
$output = "output";
if (array_key_exists("argv", $_SERVER) && sizeof($_SERVER['argv']) >= 3 && $_SERVER['argv'][2] != NULL)
	$output = $_SERVER['argv'][2];

fputs(STDOUT, "[$_apptitle] Reading input file\n");
if (!file_exists($input))
	throw new FileNotFoundException($input);

fputs(STDOUT, "[$_apptitle] Parsing input file\n");
$data = json_decode(file_get_contents($input));

if (!file_exists($output))
	mkdir($output);

fputs(STDOUT, "[$_apptitle] Checking input datas\n");
check_data($data);

//: REMPLISSAGE DES TABLEAUX $_documented*
fputs(STDOUT, "[$_apptitle] Filling references arrays\n");
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		ObjectsFinder::registerPackage($actualPackage->name);
		
		if (property_exists($actualPackage, "functions") && $actualPackage->functions != NULL) {
			foreach ($actualPackage->functions as $actualFunction)
				ObjectsFinder::registerFunction($actualPackage->name, $actualFunction->name);
		}
		
		if (property_exists($actualPackage, "classes") && $actualPackage->classes != NULL) {
			foreach ($actualPackage->classes as $actualClass)
				ObjectsFinder::registerClass($actualPackage->name, $actualClass->name);
		}
		
		if (property_exists($actualPackage, "interfaces") && $actualPackage->interfaces != NULL) {
			foreach ($actualPackage->interfaces as $actualInterface)
				ObjectsFinder::registerInterface($actualPackage->name, $actualInterface->name);
		}
		
		if (property_exists($actualPackage, "variables") && $actualPackage->variables != NULL) {
			foreach ($actualPackage->variables as $actualVariable)
				ObjectsFinder::registerVariable($actualPackage->name, $actualVariable->name);
		}
		
		if (property_exists($actualPackage, "annotations") && $actualPackage->annotations != NULL) {
			foreach ($actualPackage->annotations as $actualAnnotation)
				ObjectsFinder::registerAnnotation($actualPackage->name, $actualAnnotation->name);
		}
		
		if (property_exists($actualPackage, "preprocessorConstants") && $actualPackage->preprocessorConstants != NULL) {
			foreach ($actualPackage->preprocessorConstants as $actualPreprocessorConstant)
				ObjectsFinder::registerPreprocessorConstant($actualPackage->name, $actualPreprocessorConstant->name);
		}
		
		if (property_exists($actualPackage, "suitcases") && $actualPackage->suitcases != NULL) {
			foreach ($actualPackage->suitcases as $actualSuitcase)
				ObjectsFinder::registerEnum($actualPackage->name, $actualSuitcase->name);
		}
		
		if (property_exists($actualPackage, "typedefs") && $actualPackage->typedefs != NULL) {
			foreach ($actualPackage->typedefs as $actualTypedef)
				ObjectsFinder::registerTypedef($actualPackage->name, $actualTypedef->name);
		}
	}
}

// on copie la base
fputs(STDOUT, "[$_apptitle] Creating base\n");
if (!file_exists("$output/css"))
	mkdir("$output/css");
if (!file_exists("$output/fonts"))
	mkdir("$output/fonts");
if (!file_exists("$output/js"))
	mkdir("$output/js");
if (!file_exists("$output/elements"))
	mkdir("$output/elements");
if (!file_exists("docMakerBase"))
	throw new FileNotFoundException("docMakerBase directory is not found");
copy("docMakerBase/css/style.css", "$output/css/style.css");
copy("docMakerBase/fonts/glyphicons-halflings-regular.eot", "$output/fonts/glyphicons-halflings-regular.eot");
copy("docMakerBase/fonts/glyphicons-halflings-regular.svg", "$output/fonts/glyphicons-halflings-regular.svg");
copy("docMakerBase/fonts/glyphicons-halflings-regular.ttf", "$output/fonts/glyphicons-halflings-regular.ttf");
copy("docMakerBase/fonts/glyphicons-halflings-regular.woff", "$output/fonts/glyphicons-halflings-regular.woff");
copy("docMakerBase/fonts/glyphicons-halflings-regular.woff2", "$output/fonts/glyphicons-halflings-regular.woff2");
copy("docMakerBase/js/bootstrap.min.js", "$output/js/bootstrap.min.js");
copy("docMakerBase/js/jquery.js", "$output/js/jquery.js");
copy("docMakerBase/js/npm.js", "$output/js/npm.js");
$_filesCreatedCount += 9;

//: CREATION DU FICHIER index.html
fputs(STDOUT, "[$_apptitle] Creating file index.html\n");
$generalInfosTable = [];
$file_index = "<!DOCTYPE HTML>
<html>
	<head>
		<title>Documentation</title>
		<meta charset=\"utf-8\"/>
		<link rel=stylesheet type=\"text/css\" href=\"css/style.css\"/>
	</head>
	<body>
		";
$file_index .= getHeaderMenu(false);
if (property_exists($data, "name") && $data->name != NULL && $data->name != "")
	$file_index .= "
		<div class=\"page-header\">
			<h1>" . $data->name . "</h1>
		</div>";
if (property_exists($data, "description") && $data->description != NULL && $data->description != "")
	$file_index .= "
		<p>" . $data->description . "</p><br/>";
if (property_exists($data, "version") && $data->version != NULL && $data->version != "")
	$generalInfosTable[Lex::getVersion()] = $data->version;
if (property_exists($data, "author") && $data->author != NULL && $data->author != "")
	$generalInfosTable[Lex::getAuteur()] = $data->author;
if (!empty($generalInfosTable))
	$file_index .= table($generalInfosTable);
$file_index .= "	</body>
</html>";
file_put_contents("$output/index.html", $file_index);
unset($generalInfosTable);
$_filesCreatedCount++;

//: CREATION DU FICHIER packages.html
fputs(STDOUT, "[$_apptitle] Creating file packages.html\n");
$file_packages = "<!DOCTYPE HTML>
<html>
	<head>
		<title>Packages</title>
		<meta charset=\"utf-8\"/>
		<link rel=stylesheet type=\"text/css\" href=\"css/style.css\"/>
	</head>
	<body>
		";
$file_packages .= getHeaderMenu(false);
$file_packages .= "
		<div class=\"page-header\">
			<h1>Packages</h1>
		</div>";
if (property_exists($data, "packages") && $data->packages != NULL) {
	$file_packages .= "
		<ul>";
	foreach ($data->packages as $i) {
		$shownDescr = "";
		
		if (property_exists($i, "description") && $i->description != NULL && $i->description != "") {
			$shownDescr = " : ";
			if (strlen($i->description) < $_DESCRMAXLEN)
				$shownDescr .= $i->description;
			else {
				for ($count = 0; $count < $_DESCRMAXLEN; $count++)
					$shownDescr .= $i->description[$count];
				$shownDescr .= "...";
			}
		}
		
		$lblNew = "";
		if (property_exists($i, "since") && property_exists($data, "version") && $i->since == $data->version)
			$lblNew = Lex::getNewLabel();
		
		$file_packages .= "
			<li><a href=\"elements/" . $i->name . ".html\">" . $i->name . "</a>$shownDescr $lblNew</li>";
	}
}
$file_packages .= "
	</body>
</html>";
file_put_contents("$output/packages.html", $file_packages);
$_filesCreatedCount++;

//: CREATION DU FICHIER deprecated.html
fputs(STDOUT, "[$_apptitle] Creating file deprecated.html\n");
$file_deprecated = "<!DOCTYPE HTML>
<html>
	<head>
		<title>Deprecated</title>
		<meta charset=\"utf-8\"/>
		<link rel=stylesheet type=\"text/css\" href=\"css/style.css\"/>
	</head>
	<body>
		";
$file_deprecated .= getHeaderMenu(false);
$file_deprecated .= "
		<div class=\"page-header\">
			<h1>Deprecated</h1>
		</div><ul>";
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $packagesIterator) {
		if (property_exists($packagesIterator, "functions") && $packagesIterator->functions != NULL) {
			foreach ($packagesIterator->functions as $functionsIterator) {
				if (property_exists($functionsIterator, "deprecated") && $functionsIterator->deprecated)
					$file_deprecated .= "<li><a href=\"elements/" . $packagesIterator->name . "." . $functionsIterator->name . ".html\">" . $packagesIterator->name . "." . $functionsIterator->name . "</a></li>\n";
			}
		}
		if (property_exists($packagesIterator, "classes") && $packagesIterator->classes != NULL) {
			foreach ($packagesIterator->classes as $classesIterator) {
				if (property_exists($classesIterator, "deprecated") && $classesIterator->deprecated)
					$file_deprecated .= "<li><a href=\"elements/" . $packagesIterator->name . "." . $classesIterator->name . ".html\">" . $packagesIterator->name . "." . $classesIterator->name . "</a></li>\n";
			}
		}
		if (property_exists($packagesIterator, "interfaces") && $packagesIterator->interfaces != NULL) {
			foreach ($packagesIterator->interfaces as $interfacesIterator) {
				if (property_exists($interfacesIterator, "deprecated") && $interfacesIterator->deprecated)
					$file_deprecated .= "<li><a href=\"elements/" . $packagesIterator->name . "." . $interfacesIterator->name . ".html\">" . $packagesIterator->name . "." . $interfacesIterator->name . "</a></li>\n";
			}
		}
		if (property_exists($packagesIterator, "variables") && $packagesIterator->variables != NULL) {
			foreach ($packagesIterator->variables as $variablesIterator) {
				if (property_exists($variablesIterator, "deprecated") && $variablesIterator->deprecated)
					$file_deprecated .= "<li><a href=\"elements/" . $packagesIterator->name . "." . $variablesIterator->name . ".html\">" . $packagesIterator->name . "." . $variablesIterator->name . "</a></li>\n";
			}
		}
		if (property_exists($packagesIterator, "annotations") && $packagesIterator->annotations != NULL) {
			foreach ($packagesIterator->annotations as $annotationsIterator) {
				if (property_exists($annotationsIterator, "deprecated") && $annotationsIterator->deprecated)
					$file_deprecated .= "<li><a href=\"elements/" . $packagesIterator->name . "." . $annotationsIterator->name . ".html\">" . $packagesIterator->name . "." . $annotationsIterator->name . "</a></li>\n";
			}
		}
		if (property_exists($packagesIterator, "preprocessorConstants") && $packagesIterator->preprocessorConstants != NULL) {
			foreach ($packagesIterator->preprocessorConstants as $preprocessorConstantsIterator) {
				if (property_exists($preprocessorConstantsIterator, "deprecated") && $preprocessorConstantsIterator->deprecated)
					$file_deprecated .= "<li><a href=\"elements/" . $packagesIterator->name . "." . $preprocessorConstantsIterator->name . ".html\">" . $packagesIterator->name . "." . $preprocessorConstantsIterator->name . "</a></li>\n";
			}
		}
		
		if (property_exists($packagesIterator, "suitcases") && $packagesIterator->suitcases != NULL) {
			foreach ($packagesIterator->suitcases as $suitcasesIterator) {
				if (property_exists($suitcasesIterator, "deprecated") && $suitcasesIterator->deprecated)
					$file_deprecated .= "<li><a href=\"elements/" . $packagesIterator->name . "." . $suitcasesIterator->name . ".html\">" . $packagesIterator->name . "." . $suitcasesIterator->name . "</a></li>\n";
			}
		}
		
		if (property_exists($packagesIterator, "typedefs") && $packagesIterator->typedefs != NULL) {
			foreach ($packagesIterator->typedefs as $typedefsIterator) {
				if (property_exists($typedefsIterator, "deprecated") && $typedefsIterator->deprecated)
					$file_deprecated .= "<li><a href=\"elements/" . $packagesIterator->name . "." . $typedefsIterator->name . ".html\">" . $packagesIterator->name . "." . $typedefsIterator->name . "</a></li>\n";
			}
		}
	}
}
$file_deprecated .= "
		</ul>
	</body>
</html>";
file_put_contents("$output/deprecated.html", $file_deprecated);
$_filesCreatedCount++;

//: CREATION DES FICHIERS DE PACKAGES
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . ".html\n");
		$file_package = "<!DOCTYPE HTML>
<html>
	<head>
		<title>" . $actualPackage->name . "</title>
		<meta charset=\"utf-8\"/>
		<link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/>
	</head>
	<body>
		";
		$file_package .= getHeaderMenu(true);
		$file_package .= "
		<div class=\"page-header\">
			<h1>" . $actualPackage->name . " <small>" . Lex::getPackage() . "</small></h1>
		</div>
		";
		
		if (property_exists($actualPackage, "description") && $actualPackage->description != NULL && $actualPackage->description != "")
			$file_package .= "<p>" . $actualPackage->description . "</p>";
		
		if (property_exists($actualPackage, "since") && $actualPackage->since != NULL && $actualPackage->since != "")
			$lst[Lex::getVersionMin()] = $actualPackage->since;
		
		if (!empty($lst)) {
			$file_package .= table($lst, Lex::getInformations());
			$file_package .= "<br/>";
			$lst = [];
		}
		
		$file_package .= "<br/><h2>Contenu</h2>";
		if (property_exists($actualPackage, "functions") && $actualPackage->functions != NULL) {
			$lstFnts = [];
			$elementsCount = 0;
			
			foreach ($actualPackage->functions as $actualFunction) {
				$elementsCount++;
				$val = "";
				
				if (property_exists($actualFunction, "shortDescription") && $actualFunction->shortDescription != NULL && $actualFunction->shortDescription != "") {
					$val = $actualFunction->shortDescription;
				} elseif (property_exists($actualFunction, "description") && $actualFunction->description != NULL && $actualFunction->description != "") {
					if (strlen($actualFunction->description) < $_DESCRMAXLEN)
						$val = $actualFunction->description;
					else {
						for ($count = 0; $count < $_DESCRMAXLEN; $count++)
							$val .= $actualFunction->description[$count];
						$val .= "...";
					}
				}
				
				$labelNew = "";
				if (property_exists($actualFunction, "since") && property_exists($data, "version") && $actualFunction->since == $data->version)
					$labelNew = " " . Lex::getNewLabel();
				
				$lstFnts["<code><a href=\"" . $actualPackage->name . "." . $actualFunction->name . ".html\">" . $actualFunction->name . "</a></code>" . $labelNew] = $val;
			}
			$file_package .= table($lstFnts, Lex::getFonctions() . " <span class=\"badge\">$elementsCount</span>");
			$file_package .= "<br/>";
		}
		if (property_exists($actualPackage, "classes") && $actualPackage->classes != NULL) {
			$lstCls = [];
			$elementsCount = 0;
			
			foreach ($actualPackage->classes as $actualClass) {
				$val = "";
				$elementsCount++;
				
				if (property_exists($actualClass, "shortDescription") && $actualClass->shortDescription != NULL && $actualClass->shortDescription != "")
					$val = $actualClass->shortDescription;
				elseif (property_exists($actualClass, "description") && $actualClass->description != NULL && $actualClass->description != "") {
					if (strlen($actualClass->description) < $_DESCRMAXLEN)
						$val = $actualClass->description;
					else {
						for ($count = 0; $count < $_DESCRMAXLEN; $count++)
							$val .= $actualClass->description[$count];
						$val .= "...";
					}
				}
				
				$labelNew = "";
				if (property_exists($actualClass, "since") && property_exists($data, "version") && $actualClass->since == $data->version)
					$labelNew = " " . Lex::getNewLabel();
				
				$lstCls["<code><a href=\"" . $actualPackage->name . "." . $actualClass->name . ".html\">" . $actualClass->name . "</a></code>" . $labelNew] = $val;
			}
			$file_package .= table($lstCls, Lex::getClasses() . " <span class=\"badge\">$elementsCount</span>");
			$file_package .= "<br/>";
		}
		if (property_exists($actualPackage, "interfaces") && $actualPackage->interfaces != NULL) {
			$lstItfs = [];
			$elementsCount = 0;
			
			foreach ($actualPackage->interfaces as $actualInterface) {
				$val = "";
				$elementsCount++;
				
				if (property_exists($actualInterface, "shortDescription") && $actualInterface->shortDescription != NULL && $actualInterface->shortDescription != "")
					$val = $actualInterface->shortDescription;
				elseif (property_exists($actualInterface, "description") && $actualInterface->description != NULL && $actualInterface->description != "") {
					if (strlen($actualInterface->description) < $_DESCRMAXLEN)
						$val = $actualInterface->description;
					else {
						for ($count = 0; $count < $_DESCRMAXLEN; $count++)
							$val .= $actualInterface->description[$count];
						$val .= "...";
					}
				}
				
				$labelNew = "";
				if (property_exists($actualInterface, "since") && property_exists($data, "version") && $actualInterface->since == $data->version)
					$labelNew = " " . Lex::getNewLabel();
				
				$lstItfs["<code><a href=\"" . $actualPackage->name . "." . $actualInterface->name . ".html\">" . $actualInterface->name . "</a></code>" . $labelNew] = $val;
			}
			$file_package .= table($lstItfs, Lex::getInterfaces() . " <span class=\"badge\">$elementsCount</span>");
			$file_package .= "<br/>";
		}
		if (property_exists($actualPackage, "variables") && $actualPackage->variables != NULL) {
			$lstVars = [];
			$elementsCount = 0;
			
			foreach ($actualPackage->variables as $actualVariable) {
				$val = "";
				$elementsCount++;
				
				if (property_exists($actualVariable, "description") && $actualVariable->description != NULL && $actualVariable->description != "") {
					if (strlen($actualVariable->description) < $_DESCRMAXLEN)
						$val = $actualVariable->description;
					else {
						for ($count = 0; $count < $_DESCRMAXLEN; $count++)
							$val .= $actualVariable->description[$count];
						$val .= "...";
					}
				}
				
				$labelNew = "";
				if (property_exists($actualVariable, "since") && property_exists($data, "version") && $actualVariable->since == $data->version)
					$labelNew = " " . Lex::getNewLabel();
				
				$lstVars["<code><a href=\"" . $actualPackage->name . "." . $actualVariable->name . ".html\">" . $actualVariable->name . "</a></code>" . $labelNew] = $val;
			}
			$file_package .= table($lstVars, Lex::getVariables() . " <span class=\"badge\">$elementsCount</span>");
			$file_package .= "<br/>";
		}
		if (property_exists($actualPackage, "annotations") && $actualPackage->annotations != NULL) {
			$lstAnnots = [];
			$elementsCount = 0;
			
			foreach ($actualPackage->annotations as $actualAnnotation) {
				$val = "";
				$elementsCount++;
				
				if (property_exists($actualAnnotation, "shortDescription") && $actualAnnotation->shortDescription != NULL && $actualAnnotation->shortDescription != "")
					$val = $actualAnnotation->shortDescription;
				elseif (property_exists($actualAnnotation, "description") && $actualAnnotation->description != NULL && $actualAnnotation->description != "") {
					if (strlen($actualAnnotation->description) < $_DESCRMAXLEN)
						$val = $actualAnnotation->description;
					else {
						for ($count = 0; $count < $_DESCRMAXLEN; $count++)
							$val .= $actualAnnotation->description[$count];
						$val .= "...";
					}
				}
				
				$labelNew = "";
				if (property_exists($actualAnnotation, "since") && property_exists($data, "version") && $actualAnnotation->since == $data->version)
					$labelNew = " " . Lex::getNewLabel();
				
				$lstAnnots["<code><a href=\"" . $actualPackage->name . "." . $actualAnnotation->name . ".html\">" . $actualAnnotation->name . "</a></code>" . $labelNew] = $val;
			}
			$file_package .= table($lstAnnots, Lex::getAnnotations() . " <span class=\"badge\">$elementsCount</span>");
			$file_package .= "<br/>";
		}
		if (property_exists($actualPackage, "preprocessorConstants") && $actualPackage->preprocessorConstants != NULL) {
			$lstPc = [];
			$elementsCount = 0;
			
			foreach ($actualPackage->preprocessorConstants as $actualPreprocessorConstant) {
				$val = "";
				$elementsCount++;
				
				if (property_exists($actualPreprocessorConstant, "description") && $actualPreprocessorConstant->description != NULL && $actualPreprocessorConstant->description != "") {
					if (strlen($actualPreprocessorConstant->description) < $_DESCRMAXLEN)
						$val = $actualPreprocessorConstant->description;
					else {
						for ($count = 0; $count < $_DESCRMAXLEN; $count++)
							$val .= $actualPreprocessorConstant->description[$count];
						$val .= "...";
					}
				}
				
				$labelNew = "";
				if (property_exists($actualPreprocessorConstant, "since") && property_exists($data, "version") && $actualPreprocessorConstant->since == $data->version)
					$labelNew = " " . Lex::getNewLabel();
				
				$lstPc["<code><a href=\"" . $actualPackage->name . "." . $actualPreprocessorConstant->name . ".html\">" . $actualPreprocessorConstant->name . "</a></code>" . $labelNew] = $val;
			}
			$file_package .= table($lstPc, Lex::getConstantesDePreprocesseur() . " <span class=\"badge\">$elementsCount</span>");
			$file_package .= "<br/>";
		}
		if (property_exists($actualPackage, "suitcases") && $actualPackage->suitcases != NULL) {
			$lstSu = [];
			$elementsCount = 0;
			
			foreach ($actualPackage->suitcases as $actualSuitcase) {
				$val = "";
				$elementsCount++;
				
				if (property_exists($actualSuitcase, "shortDescription") && $actualSuitcase->shortDescription != NULL && $actualSuitcase->shortDescription != "")
					$val = $actualSuitcase->shortDescription;
				elseif (property_exists($actualSuitcase, "description") && $actualSuitcase->description != NULL && $actualSuitcase->description != "") {
					if (strlen($actualSuitcase->description) < $_DESCRMAXLEN)
						$val = $actualSuitcase->description;
					else {
						for ($count = 0; $count < $_DESCRMAXLEN; $count++)
							$val .= $actualSuitcase->description[$count];
						$val .= "...";
					}
				}
				
				$labelNew = "";
				if (property_exists($actualSuitcase, "since") && property_exists($data, "version") && $actualSuitcase->since == $data->version)
					$labelNew = " " . Lex::getNewLabel();
				
				$lstSu["<code><a href=\"" . $actualPackage->name . "." . $actualSuitcase->name . ".html\">" . $actualSuitcase->name . "</a></code>" . $labelNew] = $val;
			}
			$file_package .= table($lstSu, Lex::getSuitcases() . " <span class=\"badge\">$elementsCount</span>");
			$file_package .= "<br/>";
		}
		if (property_exists($actualPackage, "typedefs") && $actualPackage->typedefs != NULL) {
			$lstTd = [];
			$elementsCount = 0;
			
			foreach ($actualPackage->typedefs as $actualTypedef) {
				$val = "";
				$elementsCount++;
				
				if (property_exists($actualTypedef, "description") && $actualTypedef->description != NULL && $actualTypedef->description != "") {
					if (strlen($actualTypedef->description) < $_DESCRMAXLEN)
						$val = $actualTypedef->description;
					else {
						for ($count = 0; $count < $_DESCRMAXLEN; $count++)
							$val .= $actualTypedef->description[$count];
						$val .= "...";
					}
				}
				
				$labelNew = "";
				if (property_exists($actualTypedef, "since") && property_exists($data, "version") && $actualTypedef->since == $data->version)
					$labelNew = " " . Lex::getNewLabel();
				
				$lstTd["<code><a href=\"" . $actualPackage->name . "." . $actualTypedef->name . ".html\">" . $actualTypedef->name . "</a></code>" . $labelNew] = $val;
			}
			$file_package .= table($lstTd, Lex::getTypedefs() . " <span class=\"badge\">$elementsCount</span>");
			$file_package .= "<br/>";
		}
		
		$file_package .= "</body></html>";
		file_put_contents("$output/elements/" . $actualPackage->name . ".html", $file_package);
		$_filesCreatedCount++;
	}
}

//: CREATION DES FICHIERS DE FONCTION
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "functions") && $actualPackage->functions) {
			foreach ($actualPackage->functions as $actualFunction) {
				fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualFunction->name . ".html\n");
				$file_function = "<!DOCTYPE HTML><html><head><title>" . $actualFunction->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/></head><body>";
				$file_function .= getHeaderMenu(true);
				$file_function .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li class=\"active\">" . $actualFunction->name . "</li></ol>";
				$file_function .= "<div class=\"page-header\"><h1>" . $actualFunction->name . "<small> " . 	Lex::getFonction() . "</small></h1></div>";
				
				if ($actualFunction->deprecated)
					$file_function .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
				
				if (property_exists($actualFunction, "shortDescription") && $actualFunction->shortDescription != NULL && $actualFunction->shortDescription != "")
					$file_function .= "<p>" . $actualFunction->shortDescription . "</p>";
				if (property_exists($actualFunction, "description") && $actualFunction->description != NULL && $actualFunction->description != "")
					$file_function .= "<p>" . $actualFunction->description . "</p>";
				
				$lst[Lex::getValeurDeRetour()] = ObjectsFinder::renderObject($actualFunction->return, ObjectsFinder::$TYPE_STD);
				if (property_exists($actualFunction, "ref") && $actualFunction->ref)
					$lst[Lex::getRetourneUneReference()] = Lex::getOui();
				if (property_exists($actualFunction, "pointer") && $actualFunction->pointer)
					$lst[Lex::getRetourneUnPointeur()] = Lex::getOui();
				if (property_exists($actualFunction, "since")) {
					if ($actualFunction->since != NULL && $actualFunction->since != "")
						$lst[Lex::getVersionMin()] = $actualFunction->since;
				}
				if (property_exists($actualFunction, "author")) {
					if ($actualFunction->author != NULL && $actualFunction->author != "")
						$lst[Lex::getAuteur()] = $actualFunction->author;
				}
				if (property_exists($actualFunction, "annotations") && $actualFunction->annotations != NULL) {
					$lst[Lex::getAnnotations(false)] = ""; // pour supprimer le notice
					foreach ($actualFunction->annotations as $actualAnnotation) {
						if (is_string($actualAnnotation)) {
							$lst[Lex::getAnnotations(false)] .= ObjectsFinder::renderObject($actualAnnotation, ObjectsFinder::$TYPE_ANNOTATION) . "<br/>";
						} elseif (is_array($actualAnnotation)) {
							$val = "<code>";
							$iteration = 0;
							foreach ($actualAnnotation as $inAnnotation) {
								if ($iteration == 0) {
									if (ObjectsFinder::objectExists($inAnnotation, ObjectsFinder::$TYPE_ANNOTATION))
										$val .= "<a href=\"$inAnnotation.html\">$inAnnotation</a>";
									else
										$val .= $inAnnotation;
								} else {
									if ($iteration != 1)
										$val .= ",";
									$val .= " $inAnnotation";
								}
								$iteration++;
							}
							$val .= "</code><br/>";
							$lst[Lex::getAnnotations(false)] .= $val;
						}
					}
				}
				if (property_exists($actualFunction, "throws") && $actualFunction->throws != NULL) {
					$val = "";
					foreach ($actualFunction->throws as $actualThrow) {
						$val .= ObjectsFinder::renderObject($actualThrow->name, ObjectsFinder::$TYPE_THROW);
						if (property_exists($actualThrow, "description") && $actualThrow->description != NULL && $actualThrow->description != "")
							$val .= " : " . $actualThrow->description . "<br/>";
					}
					$lst[Lex::getExceptions()] = $val;
				}
				if (property_exists($actualFunction, "template") && $actualFunction->template)
					$lst[Lex::getTemplate()] = Lex::getOui();
				if (!empty($lst)) {
					$file_function .= "<br/>" . table($lst, Lex::getInformations());
					$lst = [];
				}
				
				if (property_exists($actualFunction, "args") && $actualFunction->args != NULL) {
					foreach ($actualFunction->args as $actualArg) {
						$val = "";
						$key = "<code>";
						$keyModifiersCount = 0;
						
						if (property_exists($actualArg, "description")) {
							$val .= $actualArg->description;
						}
						
						$key .= ObjectsFinder::renderObject($actualArg->type, ObjectsFinder::$TYPE_TYPE, false) . " ";
						
						if (property_exists($actualArg, "const") && $actualArg->const) {
							if ($keyModifiersCount <= 0)
								$key .= "const";
							else
								$key .= " const";
							$keyModifiersCount++;
						}
						
						if (property_exists($actualArg, "ref") && $actualArg->ref) {
							if ($keyModifiersCount <= 0)
								$key .= "&";
							else
								$key .= "&";
							$keyModifiersCount++;
						}
						
						if (property_exists($actualArg, "pointer") && $actualArg->pointer) {
							if ($keyModifiersCount <= 0)
								$key .= "*";
							else
								$key .= "*";
							$keyModifiersCount++;
						}
						
						if ($keyModifiersCount <= 0)
							$key .= $actualArg->name;
						else
							$key .= " " . $actualArg->name;
						
						if (property_exists($actualArg, "default")) {
							if ($actualArg->default != NULL && $actualArg->default != "") {
								if ($keyModifiersCount <= 0)
									$key .= "= " . $actualArg->default;
								else
									$key .= " = " . $actualArg->default;
								$keyModifiersCount++;
							}
						}
						
						$key .= "</code>";
						
						$lst[$key] = $val;
					}
					
					$file_function .= "<br/>";
					$file_function .= table($lst, Lex::getArguments());
					$lst = [];
				}
				
				if (property_exists($actualFunction, "template") && $actualFunction->template && property_exists($actualFunction, "template_args") && $actualFunction->template_args != NULL) {
					foreach ($actualFunction->template_args as $actualArg) {
						$val = "";
						$key = "<code>" . $actualArg->name;
						$keyModifiersCount = 1;
						
						if (property_exists($actualArg, "default")) {
							if ($actualArg->default != NULL && $actualArg->default != "") {
								if ($keyModifiersCount <= 0)
									$key .= "= " . $actualArg->default;
								else
									$key .= " = " . $actualArg->default;
								$keyModifiersCount++;
							}
						}
						
						$key .= "</code>";
						
						if (property_exists($actualArg, "description") && $actualArg->description != NULL && $actualArg->description != "")
							$val .= "<p>" . $actualArg->description . "</p>";
						
						$lst[$key] = $val;
					}
					
					$file_function .= "<br/>";
					$file_function .= table($lst, Lex::getTemplateArgs(false));
					$lst = [];
				}
				
				$file_function .= "</body></html>";
				file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualFunction->name . ".html", $file_function);
				$_filesCreatedCount++;
			}
		}
	}
}

//: CREATION DES FICHIERS DE CLASSES
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "classes") && $actualPackage->classes != NULL) {
			foreach ($actualPackage->classes as $actualClass) {
				fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualClass->name . ".html\n");
				$file_class = "<!DOCTYPE HTML><html><head><title>" . $actualClass->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet href=\"../css/style.css\" type=\"text/css\"/></head><body>";
				$file_class .= getHeaderMenu(true);
				$file_class .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li class=\"active\">" . $actualClass->name . "</li></ol>";
				$file_class .= "<div class=\"page-header\"><h1>" . $actualClass->name . "<small> " . Lex::getClass() . "</small></h1></div>";
				
				if ($actualClass->deprecated)
					$file_class .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
				
				if (property_exists($actualClass, "shortDescription") && $actualClass->shortDescription != NULL && $actualClass->shortDescription != "")
					$file_class .= "<p>" . $actualClass->shortDescription . "</p>";
				if (property_exists($actualClass, "description") && $actualClass->description != NULL && $actualClass->description != "")
					$file_class .= "<p>" . $actualClass->description . "</p>";
				
				$lst = [];
				if (property_exists($actualClass, "abstract") && $actualClass->abstract)
					$lst[Lex::getAbstrait(Lex::$GENRE_F)] = Lex::getOui();
				if (property_exists($actualClass, "final") && $actualClass->final)
					$lst[Lex::getFinal(Lex::$GENRE_F)] = Lex::getOui();
				if (property_exists($actualClass, "since") && $actualClass->since != NULL && $actualClass->since != "")
					$lst[Lex::getVersionMin()] = $actualClass->since;
				if (property_exists($actualClass, "author") && $actualClass->author != NULL && $actualClass->author != "")
					$lst[Lex::getAuteur()] = $actualClass->author;
				if (property_exists($actualClass, "extends") && $actualClass->extends != NULL) {
					$val = "";
					
					foreach ($actualClass->extends as $actualExtends)
						$val .= ObjectsFinder::renderObject($actualExtends, ObjectsFinder::$TYPE_EXTEND) . "<br/>";
					
					$lst[Lex::getEtends()] = $val;
				}
				if (property_exists($actualClass, "implements") && $actualClass->implements != NULL) {
					$lst[Lex::getImplemente()] = "";
					foreach ($actualClass->implements as $actualImplements)
						$lst[Lex::getImplemente()] .= ObjectsFinder::renderObject($actualImplements, ObjectsFinder::$TYPE_IMPLEMENTATION) . "<br/>";
				}
				if (property_exists($actualClass, "annotations") && $actualClass->annotations != NULL) {
					$lst[Lex::getAnnotations(false)] = ""; // pour supprimer le notice
					foreach ($actualClass->annotations as $actualAnnotation) {
						if (is_string($actualAnnotation))
							$lst[Lex::getAnnotations(false)] .= ObjectsFinder::renderObject($actualAnnotation, ObjectsFinder::$TYPE_ANNOTATION) . "<br/>";
						elseif (is_array($actualAnnotation)) {
							$val = "<code>";
							$iteration = 0;
							foreach ($actualAnnotation as $inAnnotation) {
								if ($iteration == 0)
									$val .= ObjectsFinder::renderObject($inAnnotation, ObjectsFinder::$TYPE_ANNOTATION, false);
								else {
									if ($iteration != 1)
										$val .= ",";
									$val .= " $inAnnotation";
								}
								$iteration++;
							}
							$val .= "</code><br/>";
							$lst[Lex::getAnnotations(false)] .= $val;
						}
					}
				}
				
				if (!empty($lst)) {
					$file_class .= "<br/>";
					$file_class .= table($lst, Lex::getInformations());
					$lst = [];
				}
				
				if (property_exists($actualClass, "members") && $actualClass->members != NULL) {
					$elementsCount = 0;
					
					foreach ($actualClass->members as $actualMember) {
						$modifiersCount = 0;
						$val = "";
						$key = "<code>";
						$elementsCount++;
						
						$key .= (property_exists($actualMember, "scope")) ? $actualMember->scope . " " : "$_DEFAULTSCOPE ";
						
						if (property_exists($actualMember, "static") && $actualMember->static) {
							if ($modifiersCount > 0)
								$key .= " static";
							else
								$key .= "static";
							$modifiersCount++;
						}
						
						if (property_exists($actualMember, "final") && $actualMember->final) {
							if ($modifiersCount > 0)
								$key .= " final";
							else
								$key .= "final";
							$modifiersCount++;
						}
						
						if (property_exists($actualMember, "const") && $actualMember->const) {
							if ($modifiersCount > 0)
								$key .= " const";
							else
								$key .= "const";
							$modifiersCount++;
						}
						
						if ($modifiersCount > 0)
							$key .= " " . ObjectsFinder::renderObject($actualMember->type, ObjectsFinder::$TYPE_TYPE, false);
						else
							$key .= ObjectsFinder::renderObject($actualMember->type, ObjectsFinder::$TYPE_TYPE, false);
						$modifiersCount++;
						
						if (property_exists($actualMember, "pointer") && $actualMember->pointer) {
							if ($modifiersCount > 0)
								$key .= " *";
							else
								$key .= "*";
							// pas de $modifiersCount++ car on veut que l'* soit collée au nom
						}
						
						if ($modifiersCount > 0)
							$key .= " " . $actualMember->name;
						else
							$key .= $actualMember->name;
						
						$key .= "</code>";
						
						if (property_exists($actualMember, "since") && property_exists($data, "version") && $actualMember->since == $data->version)
							$val .= Lex::getNewLabel() . "<br/>";
						
						if (property_exists($actualMember, "description") && $actualMember->description != NULL && $actualMember->description != "")
							$val .= $actualMember->description;
						
						if (property_exists($actualMember, "since") && $actualMember->since != NULL && $actualMember->since != "")
							$val .= "<br/><b>" . Lex::getVersionMin() . " : </b>" . $actualMember->since;
						
						if (property_exists($actualMember, "author") && $actualMember->author != NULL && $actualMember->author != "")
							$val .= "<br/><b>" . Lex::getAuteur() . " : </b>" . $actualMember->author;
						
						if (property_exists($actualMember, "deprecated") && $actualMember->deprecated)
							$val .= "<br/><div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
						
						if (property_exists($actualMember, "annotations") && $actualMember->annotations != NULL) {
							$val .= "<br/><b>" . Lex::getAnnotations(false) . " : </b><br/>";
							foreach ($actualMember->annotations as $actualAnnotation) {
								if (is_string($actualAnnotation))
									$val .= ObjectsFinder::renderObject($actualAnnotation, ObjectsFinder::$TYPE_ANNOTATION) . "<br/>";
								elseif (is_array($actualAnnotation)) {
									$v = "<code>";
									$iteration = 0;
									foreach ($actualAnnotation as $inAnnotation) {
										if ($iteration == 0)
											$v .= ObjectsFinder::renderObject($inAnnotation, ObjectsFinder::$TYPE_ANNOTATION, false);
										else {
											if ($iteration != 1)
												$v .= ",";
											$v .= " $inAnnotation";
										}
										$iteration++;
									}
									$v .= "</code><br/>";
									$val .= $v;
								}
							}
						}
						
						$lst[$key] = $val;
					}
					
					$file_class .= "<br/>";
					$file_class .= table($lst, Lex::getMembres() . " <span class=\"badge\">$elementsCount</span>");
					$lst = [];
				}
				
				if (property_exists($actualClass, "methods") && $actualClass->methods != NULL) {
					// les constructeurs
					$elementsCount = 0;
					
					foreach ($actualClass->methods as $actualMethod) {
						if (property_exists($actualMethod, "constructor") && $actualMethod->constructor) {
							$val = "";
							$key = "<code><a href=\"" . $actualPackage->name . "." . $actualClass->name . "." . $actualMethod->name . ".html\">" . $actualMethod->name;
							$elementsCount++;
							
							if (property_exists($actualMethod, "shortDescription") && $actualMethod->shortDescription != NULL && $actualMethod->shortDescription != "")
								$val = $actualMethod->shortDescription;
							elseif (property_exists($actualMethod, "description") && $actualMethod->description != NULL && $actualMethod->description != "") {
								if (strlen($actualMethod->description) < $_DESCRMAXLEN)
									$val = $actualMethod->description;
								else {
									for ($count = 0; $count < $_DESCRMAXLEN; $count++)
										$val .= $actualMethod->description[$count];
									$val .= "...";
								}
							}
							
							$lblNew = (property_exists($actualMethod, "since") && property_exists($data, "version") && $actualMethod->since == $data->version) ? " " . Lex::getNewLabel() : "";
							$key .= "</a></code>$lblNew";
							$lst[$key] = $val;
						}
					}
					
					if (!empty($lst)) {
						$file_class .= "<br/>";
						$file_class .= table($lst, Lex::getConstructeurs() . " <span class=\"badge\">$elementsCount</span>");
						$lst = [];
					}
					
					// les destructeurs
					$elementsCount = 0;
					
					foreach ($actualClass->methods as $actualMethod) {
						if (property_exists($actualMethod, "destructor") && $actualMethod->destructor) {
							$val = "";
							$key = "<code><a href=\"" . $actualPackage->name . "." . $actualClass->name . "." . $actualMethod->name . ".html\">" . $actualMethod->name;
							$elementsCount++;
							
							if (property_exists($actualMethod, "shortDescription") && $actualMethod->shortDescription != NULL && $actualMethod->shortDescription != "")
								$val = $actualMethod->shortDescription;
							elseif (property_exists($actualMethod, "description") && $actualMethod->description != NULL && $actualMethod->description != "") {
								if (strlen($actualMethod->description) < $_DESCRMAXLEN)
									$val = $actualMethod->description;
								else {
									for ($count = 0; $count < $_DESCRMAXLEN; $count++)
										$val .= $actualMethod->description[$count];
									$val .= "...";
								}
							}
							
							$lblNew = (property_exists($actualMethod, "since") && property_exists($data, "version") && $actualMethod->since == $data->version) ? " " . Lex::getNewLabel() : "";
							$key .= "</a></code>$lblNew";
							$lst[$key] = $val;
						}
					}
					
					if (!empty($lst)) {
						$file_class .= "<br/>";
						$file_class .= table($lst, Lex::getDestructeurs() . " <span class=\"badge\">$elementsCount</span>");
						$lst = [];
					}
					
					// les méthodes
					$elementsCount = 0;
					
					foreach ($actualClass->methods as $actualMethod) {
						$val = "";
						$key = "<code><a href=\"" . $actualPackage->name . "." . $actualClass->name . "." . $actualMethod->name . ".html\">" . $actualMethod->name;
						if (!property_exists($actualMethod, "constructor") && !property_exists($actualMethod, "destructor"))
							$elementsCount++;
						
						if (property_exists($actualMethod, "shortDescription") && $actualMethod->shortDescription != NULL && $actualMethod->shortDescription != "")
							$val = $actualMethod->shortDescription;
						elseif (property_exists($actualMethod, "description") && $actualMethod->description != NULL && $actualMethod->description != "") {
							if (strlen($actualMethod->description) < $_DESCRMAXLEN)
								$val = $actualMethod->description;
							else {
								for ($count = 0; $count < $_DESCRMAXLEN; $count++)
									$val .= $actualMethod->description[$count];
								$val .= "...";
							}
						}
						
						$lblNew = (property_exists($actualMethod, "since") && property_exists($data, "version") && $actualMethod->since == $data->version) ? " " . Lex::getNewLabel() : "";
						$key .= "</a></code>$lblNew";
						if (!property_exists($actualMethod, "constructor") && !property_exists($actualMethod, "destructor"))
							$lst[$key] = $val;
					}
					
					if (!empty($lst)) {
						$file_class .= "<br/>";
						$file_class .= table($lst, Lex::getMethodes() . " <span class=\"badge\">$elementsCount</span>");
						$lst = [];
					}
				}
				
				$file_class .= "</body></html>";
				file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualClass->name . ".html", $file_class);
				$_filesCreatedCount++;
			}
		}
	}
}

//: CREATION DES FICHIERS DE METHODES
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "classes") && $actualPackage->classes != NULL) {
			foreach ($actualPackage->classes as $actualClass) {
				if (property_exists($actualClass, "methods") && $actualClass->methods != NULL) {
					foreach ($actualClass->methods as $actualMethod) {
						fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualClass->name . "." . $actualMethod->name . ".html\n");
						$file_method = "<!DOCTYPE HTML><html><head><title>";
						$file_method .= $actualMethod->name;
						$file_method .= "</title><meta charset=\"utf-8\"/><link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/></head><body>";
						$file_method .= getHeaderMenu(true);
						$file_method .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li><a href=\"" . $actualPackage->name . "." . $actualClass->name . ".html\">" . $actualClass->name . "</a></li><li class=\"active\">" . $actualMethod->name . "</li></ol>";
						$file_method .= "<div class=\"page-header\"><h1>" . $actualMethod->name . "<small> " . Lex::getMethode() . "</small></h1></div>";
						
						if (property_exists($actualMethod, "deprecated") && $actualMethod->deprecated)
							$file_method .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
						
						$labels = [];
						if (property_exists($actualMethod, "constructor") && $actualMethod->constructor)
							$labels[] = "<span class=\"label label-info\">" . Lex::getConstructeur() . "</span>";
						if (property_exists($actualMethod, "destructor") && $actualMethod->destructor)
							$labels[] = "<span class=\"label label-info\">" . Lex::getDestructeur() . "</span>";
						if (!empty($labels)) {
							$file_method .= "<p>";
							foreach ($labels as $i)
								$file_method .= $i . " ";
							$file_method .= "</p>";
						}
						unset($labels);
						
						if (property_exists($actualMethod, "shortDescription") && $actualMethod->shortDescription != NULL && $actualMethod->shortDescription != "")
							$file_method .= "<p>" . $actualMethod->shortDescription . "</p>";
						if (property_exists($actualMethod, "description") && $actualMethod->description != NULL && $actualMethod->description != "")
							$file_method .= "<p>" . $actualMethod->description . "</p>";
						
						$lst = [];
						if ((!property_exists($actualMethod, "constructor") || !$actualMethod->constructor) &&
						    (!property_exists($actualMethod, "destructor") || !$actualMethod->destructor))
							$lst[Lex::getValeurDeRetour()] = ObjectsFinder::renderObject($actualMethod->return, ObjectsFinder::$TYPE_STD);
						if (property_exists($actualMethod, "ref") && $actualMethod->ref)
							$lst[Lex::getRetourneUneReference()] = Lex::getOui();
						if (property_exists($actualMethod, "pointer") && $actualMethod->pointer)
							$lst[Lex::getRetourneUnPointeur()] = Lex::getOui();
						$lst[Lex::getPortee()] = (property_exists($actualMethod, "scope")) ? "<code>" . $actualMethod->scope . "</code>" : "<code>$_DEFAULTSCOPE</code>";
						if (property_exists($actualMethod, "since") && $actualMethod->since != NULL && $actualMethod->since != "")
							$lst[Lex::getVersionMin()] = $actualMethod->since;
						if (property_exists($actualMethod, "author") && $actualMethod->author != NULL && $actualMethod->author != "")
							$lst[Lex::getAuteur()] = $actualMethod->author;
						if (property_exists($actualMethod, "annotations") && $actualMethod->annotations != NULL) {
							$lst[Lex::getAnnotations(false)] = ""; // pour supprimer le notice
							foreach ($actualMethod->annotations as $actualAnnotation) {
								if (is_string($actualAnnotation))
									$lst[Lex::getAnnotations(false)] .= ObjectsFinder::renderObject($actualAnnotation, ObjectsFinder::$TYPE_ANNOTATION) . "<br/>";
								elseif (is_array($actualAnnotation)) {
									$val = "<code>";
									$iteration = 0;
									foreach ($actualAnnotation as $inAnnotation) {
										if ($iteration == 0)
											$val .= ObjectsFinder::renderObject($inAnnotation, ObjectsFinder::$TYPE_ANNOTATION, false);
										else {
											if ($iteration != 1)
												$val .= ",";
											$val .= " $inAnnotation";
										}
										$iteration++;
									}
									$val .= "</code><br/>";
									$lst[Lex::getAnnotations(false)] .= $val;
								}
							}
						}
						if (property_exists($actualMethod, "throws") && $actualMethod->throws != NULL) {
							$val = "";
							foreach ($actualMethod->throws as $actualThrow) {
								$val .= ObjectsFinder::renderObject($actualThrow->name, ObjectsFinder::$TYPE_THROW);
								if (property_exists($actualThrow, "description") && $actualThrow->description != NULL && $actualThrow->description != "")
									$val .= " : " . $actualThrow->description . "<br/>";
							}
							$lst[Lex::getExceptions()] = $val;
						}
						if (property_exists($actualMethod, "static") && $actualMethod->static)
							$lst[Lex::getStatic()] = Lex::getOui();
						if (property_exists($actualMethod, "abstract") && $actualMethod->abstract)
							$lst[Lex::getAbstrait(Lex::$GENRE_F)] = Lex::getOui();
						if (!empty($lst)) {
							$file_method .= "<br/>";
							$file_method .= table($lst, Lex::getInformations());
						}
						$lst = [];
						
						if (property_exists($actualMethod, "args") && $actualMethod->args != NULL) {
							foreach ($actualMethod->args as $actualArg) {
								$keyModifiersCount = 0;
								$val = "";
								$key = "<code>";
								
								if (property_exists($actualArg, "description")) {
									$val .= $actualArg->description;
								}
								
								$key .= ObjectsFinder::renderObject($actualArg->type, ObjectsFinder::$TYPE_TYPE, false) . " ";
								
								if (property_exists($actualArg, "const") && $actualArg->const) {
									if ($keyModifiersCount <= 0)
										$key .= "const";
									else
										$key .= " const";
									$keyModifiersCount++;
								}
								
								if (property_exists($actualArg, "ref") && $actualArg->ref) {
									if ($keyModifiersCount <= 0)
										$key .= "&";
									else
										$key .= "&";
									$keyModifiersCount++;
								}
								
								if (property_exists($actualArg, "pointer") && $actualArg->pointer) {
									if ($keyModifiersCount <= 0)
										$key .= "*";
									else
										$key .= "*";
									$keyModifiersCount++;
								}
								
								if ($keyModifiersCount <= 0)
									$key .= $actualArg->name;
								else
									$key .= " " . $actualArg->name;
								
								if (property_exists($actualArg, "default")) {
									if ($actualArg->default != NULL && $actualArg->default != " ") {
										if ($keyModifiersCount <= 0)
											$key .= "= " . $actualArg->default;
										else
											$key .= " = " . $actualArg->default;
										$keyModifiersCount++;
									}
								}
								
								$key .= "</code>";
								
								$lst[$key] = $val;
							}
							
							$file_method .= "<br/>";
							$file_method .= table($lst, Lex::getArguments());
							$lst = [];
						}
						
						$file_method .= "</body></html>";
						file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualClass->name . "." . $actualMethod->name . ".html", $file_method);
						$_filesCreatedCount++;
					}
				}
			}
		}
	}
}

//: CREATION DES FICHIERS D'INTERFACES
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "interfaces") && $actualPackage->interfaces != NULL) {
			foreach ($actualPackage->interfaces as $actualInterface) {
				fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualInterface->name . ".html\n");
				$file_interface = "<!DOCTYPE HTML><html><head><title>" . $actualInterface->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet href=\"../css/style.css\" type=\"text/css\"/></head><body>";
				$file_interface .= getHeaderMenu(true);
				$file_interface .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li class=\"active\">" . $actualInterface->name . "</li></ol>";
				$file_interface .= "<div class=\"page-header\"><h1>" . $actualInterface->name . "<small> " . Lex::getInterface() . "</small></h1></div>";
				
				if (property_exists($actualInterface, "deprecated") && $actualInterface->deprecated)
					$file_interface .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
				
				if (property_exists($actualInterface, "shortDescription") && $actualInterface->shortDescription != NULL && $actualInterface->shortDescription != "")
					$file_interface .= "<p>" . $actualInterface->shortDescription . "</p>";
				if (property_exists($actualInterface, "description") && $actualInterface->description != NULL && $actualInterface->description != "")
					$file_interface .= "<p>" . $actualInterface->description . "</p>";
				
				if (property_exists($actualInterface, "since") && $actualInterface->since != NULL && $actualInterface->since != "")
					$lst[Lex::getVersionMin()] = $actualInterface->since;
				if (property_exists($actualInterface, "author") && $actualInterface->author != NULL && $actualInterface->author != "")
					$lst[Lex::getAuteur()] = $actualInterface->author;
				if (property_exists($actualInterface, "annotations") && $actualInterface->annotations != NULL) {
					$lst[Lex::getAnnotations(false)] = ""; // pour supprimer le notice
					foreach ($actualInterface->annotations as $actualAnnotation) {
						if (is_string($actualAnnotation))
							$lst[Lex::getAnnotations(false)] .= ObjectsFinder::renderObject($actualAnnotation, ObjectsFinder::$TYPE_ANNOTATION) . "<br/>";
						elseif (is_array($actualAnnotation)) {
							$val = "<code>";
							$iteration = 0;
							foreach ($actualAnnotation as $inAnnotation) {
								if ($iteration == 0)
									$val .= ObjectsFinder::renderObject($inAnnotation, ObjectsFinder::$TYPE_ANNOTATION, false);
								else {
									if ($iteration != 1)
										$val .= ",";
									$val .= " $inAnnotation";
								}
								$iteration++;
							}
							$val .= "</code><br/>";
							$lst[Lex::getAnnotations(false)] .= $val;
						}
					}
				}
				if (!empty($lst)) {
					$file_interface .= "<br/>";
					$file_interface .= table($lst, Lex::getInformations());
					$lst = [];
				}
				
				if (property_exists($actualInterface, "methods") && $actualInterface->methods != NULL) {
					// les constructeurs
					$elementsCount = 0;
					
					foreach ($actualInterface->methods as $actualMethod) {
						if (property_exists($actualMethod, "constructor") && $actualMethod->constructor) {
							$val = "";
							$key = "<code><a href=\"" . $actualPackage->name . "." . $actualInterface->name . "." . $actualMethod->name . ".html\">" . $actualMethod->name;
							$elementsCount++;
							
							if (property_exists($actualMethod, "shortDescription") && $actualMethod->shortDescription != NULL && $actualMethod->shortDescription != "")
								$val = $actualMethod->shortDescription;
							elseif (property_exists($actualMethod, "description") && $actualMethod->description != NULL && $actualMethod->description != "") {
								if (strlen($actualMethod->description) < $_DESCRMAXLEN)
									$val = $actualMethod->description;
								else {
									for ($count = 0; $count < $_DESCRMAXLEN; $count++)
										$val .= $actualMethod->description[$count];
									$val .= "...";
								}
							}
							
							$lblNew = (property_exists($actualMethod, "since") && property_exists($data, "version") && $actualMethod->since == $data->version) ? " " . Lex::getNewLabel() : "";
							
							$key .= "</a></code>$lblNew";
							$lst[$key] = $val;
						}
					}
					
					if (!empty($lst)) {
						$file_interface .= "<br/>";
						$file_interface .= table($lst, Lex::getConstructeurs() . " <span class=\"badge\">$elementsCount</span>");
						$lst = [];
					}
					
					// les destructeurs
					$elementsCount = 0;
					
					foreach ($actualInterface->methods as $actualMethod) {
						if (property_exists($actualMethod, "destructor") && $actualMethod->destructor) {
							$val = "";
							$key = "<code><a href=\"" . $actualPackage->name . "." . $actualInterface->name . "." . $actualMethod->name . ".html\">" . $actualMethod->name;
							$elementsCount++;
							
							if (property_exists($actualMethod, "shortDescription") && $actualMethod->shortDescription != NULL && $actualMethod->shortDescription != "")
								$val = $actualMethod->shortDescription;
							elseif (property_exists($actualMethod, "description") && $actualMethod->description != NULL && $actualMethod->description != "") {
								if (strlen($actualMethod->description) < $_DESCRMAXLEN)
									$val = $actualMethod->description;
								else {
									for ($count = 0; $count < $_DESCRMAXLEN; $count++)
										$val .= $actualMethod->description[$count];
									$val .= "...";
								}
							}
							
							$lblNew = (property_exists($actualMethod, "since") && property_exists($data, "version") && $actualMethod->since == $data->version) ? " " . Lex::getNewLabel() : "";
							$key .= "</a></code>$lblNew";
							$lst[$key] = $val;
						}
					}
					
					if (!empty($lst)) {
						$file_interface .= "<br/>";
						$file_interface .= table($lst, Lex::getDestructeurs() . " <span class=\"badge\">$elementsCount</span>");
						$lst = [];
					}
					
					// les méthodes
					$elementsCount = 0;
					
					foreach ($actualInterface->methods as $actualMethod) {
						$val = "";
						$key = "<code><a href=\"" . $actualPackage->name . "." . $actualInterface->name . "." . $actualMethod->name . ".html\">" . $actualMethod->name;
						if (!property_exists($actualMethod, "constructor") && !property_exists($actualMethod, "destructor"))
							$elementsCount++;
						
						if (property_exists($actualMethod, "shortDescription") && $actualMethod->shortDescription != NULL && $actualMethod->shortDescription != "")
							$val = $actualMethod->shortDescription;
						elseif (property_exists($actualMethod, "description") && $actualMethod->description != NULL && $actualMethod->description != "") {
							if (strlen($actualMethod->description) < $_DESCRMAXLEN)
								$val = $actualMethod->description;
							else {
								for ($count = 0; $count < $_DESCRMAXLEN; $count++)
									$val .= $actualMethod->description[$count];
								$val .= "...";
							}
						}
						
						$lblNew = (property_exists($actualMethod, "since") && property_exists($data, "version") && $actualMethod->since == $data->version) ? " " . Lex::getNewLabel() : "";
						$key .= "</a></code>$lblNew";
						if (!property_exists($actualMethod, "constructor") && !property_exists($actualMethod, "destructor"))
							$lst[$key] = $val;
					}
					
					if (!empty($lst)) {
						$file_interface .= "<br/>";
						$file_interface .= table($lst, Lex::getMethodes() . " <span class=\"badge\">$elementsCount</span>");
						$lst = [];
					}
				}
				
				if (property_exists($actualInterface, "members") && $actualInterface->members != NULL) {
					$elementsCount = 0;
					
					foreach ($actualInterface->members as $actualMember) {
						$modifiersCount = 0;
						$val = "";
						$key = "<code>";
						$elementsCount++;
						
						$key .= (property_exists($actualMember, "scope")) ? $actualMember->scope . " " : "$_DEFAULTSCOPE ";
						
						if (property_exists($actualMember, "static") && $actualMember->static) {
							if ($modifiersCount > 0)
								$key .= " static";
							else
								$key .= "static";
							$modifiersCount++;
						}
						
						if (property_exists($actualMember, "final") && $actualMember->final) {
							if ($modifiersCount > 0)
								$key .= " final";
							else
								$key .= "final";
							$modifiersCount++;
						}
						
						if (property_exists($actualMember, "const") && $actualMember->const) {
							if ($modifiersCount > 0)
								$key .= " const";
							else
								$key .= "const";
							$modifiersCount++;
						}
						
						if ($modifiersCount > 0)
							$key .= " " . ObjectsFinder::renderObject($actualMember->type, ObjectsFinder::$TYPE_TYPE, false);
						else
							$key .= ObjectsFinder::renderObject($actualMember->type, ObjectsFinder::$TYPE_TYPE, false);
						$modifiersCount++;
						
						if (property_exists($actualMember, "pointer") && $actualMember->pointer) {
							if ($modifiersCount > 0)
								$key .= " *";
							else
								$key .= "*";
							// pas de $modifiersCount++ car on veut que l'* soit collée au nom
						}
						
						if ($modifiersCount > 0)
							$key .= " " . $actualMember->name;
						else
							$key .= $actualMember->name;
						
						$key .= "</code>";
						
						if (property_exists($actualMember, "since") && property_exists($data, "version") && $actualMember->since == $data->version)
							$val .= " " . Lex::getNewLabel();
						
						if (property_exists($actualMember, "description") && $actualMember->description != NULL && $actualMember->description != "")
							$val .= $actualMember->description;
						
						if (property_exists($actualMember, "since") && $actualMember->since != NULL && $actualMember->since != "")
							$val .= "<br/><b>" . Lex::getVersionMin() . " : </b>" . $actualMember->since;
						
						if (property_exists($actualMember, "author") && $actualMember->author != NULL && $actualMember->author != "")
							$val .= "<br/><b>" . Lex::getAuteur() . " : </b>" . $actualMember->author;
						
						if (property_exists($actualMember, "deprecated") && $actualMember->deprecated)
							$val .= "<br/><div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
						
						if (property_exists($actualMember, "annotations") && $actualMember->annotations != NULL) {
							$val .= "<br/><b>" . Lex::getAnnotations(false) . " : </b><br/>";
							foreach ($actualMember->annotations as $actualAnnotation) {
								if (is_string($actualAnnotation))
									$val .= ObjectsFinder::renderObject($actualAnnotation, ObjectsFinder::$TYPE_ANNOTATION) . "<br/>";
								elseif (is_array($actualAnnotation)) {
									$v = "<code>";
									$iteration = 0;
									foreach ($actualAnnotation as $inAnnotation) {
										if ($iteration == 0)
											$v .= ObjectsFinder::renderObject($inAnnotation, ObjectsFinder::$TYPE_ANNOTATION, false);
										else {
											if ($iteration != 1)
												$v .= ",";
											$v .= " $inAnnotation";
										}
										$iteration++;
									}
									$v .= "</code><br/>";
									$val .= $v;
								}
							}
						}
						
						$lst[$key] = $val;
					}
					
					$file_interface .= "<br/>";
					$file_interface .= table($lst, Lex::getMembres() . " <span class=\"badge\">$elementsCount</span>");
					$lst = [];
				}
				
				$file_interface .= "</body></html>";
				file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualInterface->name . ".html", $file_interface);
				$_filesCreatedCount++;
			}
		}
	}
}

//: CREATION DES FICHIERS DE METHODES D'INTERFACES
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "interfaces") && $actualPackage->interfaces != NULL) {
			foreach ($actualPackage->interfaces as $actualInterface) {
				if (property_exists($actualInterface, "methods") && $actualInterface->methods != NULL) {
					foreach ($actualInterface->methods as $actualMethod) {
						fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualInterface->name . "." . $actualMethod->name . ".html\n");
						$file_method = "<!DOCTYPE HTML><html><head><title>" . $actualMethod->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/></head><body>";
						$file_method .= getHeaderMenu(true);
						$file_method .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li><a href=\"" . $actualPackage->name . "." . $actualInterface->name . ".html\">" . $actualInterface->name . "</a></li><li class=\"active\">" . $actualMethod->name . "</li></ol>";
						$file_method .= "<div class=\"page-header\"><h1>" . $actualMethod->name . "<small> " . Lex::getMethode() . "</small></h1></div>";
						
						if (property_exists($actualMethod, "deprecated") && $actualMethod->deprecated)
							$file_method .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
						
						$labels = [];
						if (property_exists($actualMethod, "constructor") && $actualMethod->constructor)
							$labels[] = "<span class=\"label label-info\">" . Lex::getConstructeur() . "</span>";
						if (property_exists($actualMethod, "destructor") && $actualMethod->destructor)
							$labels[] = "<span class=\"label label-info\">" . Lex::getDestructeur() . "</span>";
						if (!empty($labels)) {
							$file_method .= "<p>";
							foreach ($labels as $i)
								$file_method .= $i . " ";
							$file_method .= "</p>";
						}
						unset($labels);
						
						if (property_exists($actualMethod, "shortDescription") && $actualMethod->shortDescription != NULL && $actualMethod->shortDescription != "")
							$file_method .= "<p>" . $actualMethod->shortDescription . "</p>";
						if (property_exists($actualMethod, "description") && $actualMethod->description != NULL && $actualMethod->description != "")
							$file_method .= "<p>" . $actualMethod->description . "</p>";
						
						if ((!property_exists($actualMethod, "constructor") || !$actualMethod->constructor) &&
						    (!property_exists($actualMethod, "destructor") || !$actualMethod->destructor))
							$lst[Lex::getValeurDeRetour()] = ObjectsFinder::renderObject($actualMethod->return, ObjectsFinder::$TYPE_STD);
						if (property_exists($actualMethod, "ref") && $actualMethod->ref)
							$lst[Lex::getRetourneUneReference()] = Lex::getOui();
						if (property_exists($actualMethod, "pointer") && $actualMethod->pointer)
							$lst[Lex::getRetourneUnPointeur()] = Lex::getOui();
						$lst[Lex::getPortee()] = (property_exists($actualMethod, "scope")) ? "<code>" . $actualMethod->scope . "</code>" : "<code>$_DEFAULTSCOPE</code>";
						if (property_exists($actualMethod, "since") && $actualMethod->since != NULL && $actualMethod->since != "")
							$lst[Lex::getVersionMin()] = $actualMethod->since;
						if (property_exists($actualMethod, "author") && $actualMethod->author != NULL && $actualMethod->author != "")
							$lst[Lex::getAuteur()] = $actualMethod->author;
						if (property_exists($actualMethod, "annotations") && $actualMethod->annotations != NULL) {
							$lst[Lex::getAnnotations(false)] = ""; // pour supprimer le notice
							foreach ($actualMethod->annotations as $actualAnnotation) {
								if (is_string($actualAnnotation))
									$lst[Lex::getAnnotations(false)] .= ObjectsFinder::renderObject($actualAnnotation, ObjectsFinder::$TYPE_ANNOTATION) . "<br/>";
								elseif (is_array($actualAnnotation)) {
									$val = "<code>";
									$iteration = 0;
									foreach ($actualAnnotation as $inAnnotation) {
										if ($iteration == 0)
											$val .= ObjectsFinder::renderObject($inAnnotation, ObjectsFinder::$TYPE_ANNOTATION, false);
										else {
											if ($iteration != 1)
												$val .= ",";
											$val .= " $inAnnotation";
										}
										$iteration++;
									}
									$val .= "</code><br/>";
									$lst[Lex::getAnnotations(false)] .= $val;
								}
							}
						}
						if (property_exists($actualMethod, "static") && $actualMethod->static)
							$lst[Lex::getStatic()] = Lex::getOui();
						if (property_exists($actualMethod, "abstract") && $actualMethod->abstract)
							$lst[Lex::getAbstrait(Lex::$GENRE_F)] = Lex::getOui();
						if (!empty($lst)) {
							$file_method .= table($lst, Lex::getInformations());
							$lst = [];
						}
						
						if (property_exists($actualMethod, "args") && $actualMethod->args != NULL) {
							foreach ($actualMethod->args as $actualArg) {
								$modifiersCount = 0;
								$val = "";
								$key = "<code>";
								
								if (property_exists($actualArg, "description")) {
									$val .= $actualArg->description;
								}
								
								if ($modifiersCount > 0)
									$key .= " " . ObjectsFinder::renderObject($actualArg->type, ObjectsFinder::$TYPE_TYPE, false);
								else
									$key .= ObjectsFinder::renderObject($actualArg->type, ObjectsFinder::$TYPE_TYPE, false);
								$modifiersCount++;
								
								if (property_exists($actualArg, "const") && $actualArg->const) {
									if ($keyModifiersCount <= 0)
										$key .= "const";
									else
										$key .= " const";
									$keyModifiersCount++;
								}
								
								if (property_exists($actualArg, "ref") && $actualArg->ref) {
									if ($keyModifiersCount <= 0)
										$key .= "&";
									else
										$key .= "&";
									$keyModifiersCount++;
								}
								
								if (property_exists($actualArg, "pointer") && $actualArg->pointer) {
									if ($keyModifiersCount <= 0)
										$key .= "*";
									else
										$key .= "*";
									$keyModifiersCount++;
								}
								
								if ($keyModifiersCount <= 0)
									$key .= $actualArg->name;
								else
									$key .= " " . $actualArg->name;
								
								if (property_exists($actualArg, "default")) {
									if ($actualArg->default != NULL && $actualArg->default != " ") {
										if ($keyModifiersCount <= 0)
											$key .= "= " . $actualArg->default;
										else
											$key .= " = " . $actualArg->default;
										$keyModifiersCount++;
									}
								}
								
								$key .= "</code>";
								
								$lst[$key] = $val;
							}
							
							$file_method .= "<br/>";
							$file_method .= table($lst, Lex::getArguments());
							$lst = [];
						}
						
						$file_method .= "</body></html>";
						file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualInterface->name . "." . $actualMethod->name . ".html", $file_method);
						$_filesCreatedCount++;
					}
				}
			}
		}
	}
}

//: CREATION DES FICHIERS DE VARIABLES
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "variables") && $actualPackage->variables != NULL) {
			foreach ($actualPackage->variables as $actualVariable) {
				fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualVariable->name . ".html\n");
				$file_variable = "<!DOCTYPE HTML><html><head><title>" . $actualVariable->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/></head><body>";
				$file_variable .= getHeaderMenu(true);
				$file_variable .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li class=\"active\">" . $actualVariable->name . "</li></ol>";
				$file_variable .= "<div class=\"page-header\"><h1>" . $actualVariable->name . "<small> " . Lex::getVariable() . "</small></h1></div>";
				
				if (property_exists($actualVariable, "deprecated") && $actualVariable->deprecated)
					$file_variable .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
				
				if (property_exists($actualVariable, "description") && $actualVariable->description != NULL && $actualVariable->description != "")
					$file_variable .= "<p>" . $actualVariable->description . "</p>";
				
				$lst[Lex::getType()] = ObjectsFinder::renderObject($actualVariable->type, ObjectsFinder::$TYPE_TYPE);
				if (property_exists($actualVariable, "since") && $actualVariable->since != NULL && $actualVariable->since != "")
					$lst[Lex::getVersionMin()] = $actualVariable->since;
				if (property_exists($actualVariable, "author") && $actualVariable->author != NULL && $actualVariable->author != "")
					$lst[Lex::getAuteur()] = $actualVariable->author;
				if (property_exists($actualVariable, "const") && $actualVariable->const)
					$lst[Lex::getConstant(Lex::$GENRE_F)] = Lex::getOui();
				if (property_exists($actualVariable, "pointer") && $actualVariable->pointer)
					$lst[Lex::getPointeur()] = Lex::getOui();
				if (property_exists($actualVariable, "annotations") && $actualVariable->annotations != NULL) {
					$lst[Lex::getAnnotations(false)] = ""; // pour supprimer le notice
					foreach ($actualVariable->annotations as $actualAnnotation) {
						if (is_string($actualAnnotation))
							$lst[Lex::getAnnotations(false)] .= ObjectsFinder::renderObject($actualAnnotation, ObjectsFinder::$TYPE_ANNOTATION) . "<br/>";
						elseif (is_array($actualAnnotation)) {
							$val = "<code>";
							$iteration = 0;
							foreach ($actualAnnotation as $inAnnotation) {
								if ($iteration == 0)
									$val .= ObjectsFinder::renderObject($inAnnotation, ObjectsFinder::$TYPE_ANNOTATION, false);
								else {
									if ($iteration != 1)
										$val .= ",";
									$val .= " $inAnnotation";
								}
								$iteration++;
							}
							$val .= "</code><br/>";
							$lst[Lex::getAnnotations(false)] .= $val;
						}
					}
				}
				if (!empty($lst)) {
					$file_variable .= "<br/>";
					$file_variable .= table($lst, Lex::getInformations());
					$lst = [];
				}
				
				$file_variable .= "</body></html>";
				file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualVariable->name . ".html", $file_variable);
				$_filesCreatedCount++;
			}
		}
	}
}

//: CREATION DES FICHIERS D'ANNOTATIONS
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "annotations") && $actualPackage->annotations != NULL) {
			foreach ($actualPackage->annotations as $actualAnnotation) {
				fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualAnnotation->name . ".html\n");
				$file_annotation = "<!DOCTYPE HTML><html><head><title>" . $actualAnnotation->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/></head><body>";
				$file_annotation .= getHeaderMenu(true);
				$file_annotation .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li class=\"active\">" . $actualAnnotation->name . "</li></ol>";
				$file_annotation .= "<div class=\"page-header\"><h1>" . $actualAnnotation->name . "<small> " . Lex::getAnnotation() . "</small></h1></div>";
				
				if (property_exists($actualAnnotation, "deprecated") && $actualAnnotation->deprecated)
					$file_annotation .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
				
				if (property_exists($actualAnnotation, "shortDescription") && $actualAnnotation->shortDescription != NULL && $actualAnnotation->shortDescription != "")
					$file_annotation .= "<p>" . $actualAnnotation->shortDescription . "</p>";
				if (property_exists($actualAnnotation, "description") && $actualAnnotation->description != NULL && $actualAnnotation->description != "")
					$file_annotation .= "<p>" . $actualAnnotation->description . "</p>";
				
				if (property_exists($actualAnnotation, "author") && $actualAnnotation->author != NULL && $actualAnnotation->author != "")
					$lst[Lex::getAuteur()] = $actualAnnotation->author;
				if (property_exists($actualAnnotation, "since") && $actualAnnotation->since != NULL && $actualAnnotation->since != "")
					$lst[Lex::getVersionMin()] = $actualAnnotation->since;
				if (!empty($lst)) {
					$file_annotation .= "<br/>";
					$file_annotation .= table($lst, Lex::getInformations());
					$lst = [];
				}
				
				if (property_exists($actualAnnotation, "args") && $actualAnnotation->args != NULL) {
					foreach ($actualAnnotation->args as $actualArg)
						$lst["<code>".(ObjectsFinder::renderObject($actualArg->type,ObjectsFinder::$TYPE_TYPE,false))." ".$actualArg->name.((property_exists($actualArg,"default")&&$actualArg->default!=NULL&&$actualArg->default!="")?" = ".$actualArg->default:"")."</code>"]=(property_exists($actualArg,"description")&&$actualArg->description!=NULL&&$actualArg->description!="")?$actualArg->description:"";
					
					if (!empty($lst)) {
						$file_annotation .= "<br/>";
						$file_annotation .= table($lst, Lex::getArguments());
						$lst = [];
					}
				}
				
				$file_annotation .= "</body></html>";
				file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualAnnotation->name . ".html", $file_annotation);
				$_filesCreatedCount++;
			}
		}
	}
}

//: CREATION DES FICHIERS DE CONSTANTES DE PREPROCESSEUR
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "preprocessorConstants") && $actualPackage->preprocessorConstants != NULL) {
			foreach ($actualPackage->preprocessorConstants as $actualPreprocessorConstant) {
				fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualPreprocessorConstant->name . ".html\n");
				$file_preprocessorConstant = "<!DOCTYPE HTML><html><head><title>" . $actualPreprocessorConstant->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/></head><body>";
				$file_preprocessorConstant .= getHeaderMenu(true);
				$file_preprocessorConstant .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li class=\"active\">" . $actualPreprocessorConstant->name . "</li></ol>";
				$file_preprocessorConstant .= "<div class=\"page-header\"><h1>" . $actualPreprocessorConstant->name . "<small> " . Lex::getConstanteDePreprocesseur() . "</small></h1></div>";
				
				if (property_exists($actualPreprocessorConstant, "deprecated") && $actualPreprocessorConstant->deprecated)
					$file_preprocessorConstant .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
				
				if (property_exists($actualPreprocessorConstant, "description") && $actualPreprocessorConstant->description != NULL && $actualPreprocessorConstant->description != "")
					$file_preprocessorConstant .= "<p>" . $actualPreprocessorConstant->description . "</p>";
				
				if (property_exists($actualPreprocessorConstant, "value") && $actualPreprocessorConstant->value != NULL && $actualPreprocessorConstant->value != "")
					$lst[Lex::getValeur()] = "<code>" . $actualPreprocessorConstant->value . "</code>";
				else
					$lst[Lex::getValeur()] = "<span class=\"label label-info\">" . Lex::getAucun(Lex::$GENRE_F) . "</span>";
				if (property_exists($actualPreprocessorConstant, "author") && $actualPreprocessorConstant->author != NULL && $actualPreprocessorConstant->author != "")
					$lst[Lex::getAuteur()] = $actualPreprocessorConstant->author;
				if (property_exists($actualPreprocessorConstant, "since") && $actualPreprocessorConstant->since != NULL && $actualPreprocessorConstant->since != "")
					$lst[Lex::getVersionMin()] = $actualPreprocessorConstant->since;
				if (!empty($lst)) {
					$file_preprocessorConstant .= "<br/>";
					$file_preprocessorConstant .= table($lst, Lex::getInformations());
					$lst = [];
				}
				
				$file_preprocessorConstant .= "</body></html>";
				file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualPreprocessorConstant->name . ".html", $file_preprocessorConstant);
				$_filesCreatedCount++;
			}
		}
	}
}

//: CREATION DES FICHIERS DE SUITCASES
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "suitcases") && $actualPackage->suitcases != NULL) {
			foreach ($actualPackage->suitcases as $actualSuitcase) {
				fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualSuitcase->name . ".html\n");
				$file_suitcase = "<!DOCTYPE HTML><html><head><title>" . $actualSuitcase->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/></head><body>";
				$file_suitcase .= getHeaderMenu(true);
				$file_suitcase .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li class=\"active\">" . $actualSuitcase->name . "</li></ol>";
				$file_suitcase .= "<div class=\"page-header\"><h1>" . $actualSuitcase->name . " <small>" . Lex::getSuitcase() . "</small></h1></div>";
				
				if (property_exists($actualSuitcase, "deprecated") && $actualSuitcase->deprecated)
					$file_suitcase .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
				
				if (property_exists($actualSuitcase, "shortDescription") && $actualSuitcase->shortDescription != NULL && $actualSuitcase->shortDescription != "")
					$file_suitcase .= "<p>" . $actualSuitcase->shortDescription . "</p>";
				if (property_exists($actualSuitcase, "description") && $actualSuitcase->description != NULL && $actualSuitcase->description != "")
					$file_suitcase .= "<p>" . $actualSuitcase->description . "</p>";
				
				if (property_exists($actualSuitcase, "since") && $actualSuitcase->since != NULL && $actualSuitcase->since != "")
					$lst[Lex::getVersionMin()] = $actualSuitcase->since;
				if (property_exists($actualSuitcase, "author") && $actualSuitcase->author != NULL && $actualSuitcase->author != "")
					$lst[Lex::getAuteur()] = $actualSuitcase->author;
				if (!empty($lst)) {
					$file_suitcase .= "<br/>";
					$file_suitcase .= table($lst, Lex::getInformations());
					$lst = [];
				}
				
				if (property_exists($actualSuitcase, "content") && $actualSuitcase->content != NULL) {
					$elementsCount = 0;
					
					foreach ($actualSuitcase->content as $actualKey) {
						$key = "<code>" . $actualKey->name . "</code>";
						$val = "";
						
						if (property_exists($actualKey, "value") && $actualKey->value != NULL && $actualKey->value != "")
							$key .= " (<code>" . $actualKey->value . "</code>)";
						
						if (property_exists($actualKey, "since") && property_exists($data, "version") && $actualKey->since == $data->version)
							$val .= " " . Lex::getNewLabel() . "<br/>";
						
						if (property_exists($actualKey, "description") && $actualKey->description != NULL && $actualKey->description != "")
							$val .= $actualKey->description;
						
						if (property_exists($actualKey, "since") && $actualKey->since != NULL && $actualKey->since != "")
							$val .= "<br/><b>" . Lex::getVersionMin() . "</b> : " . $actualKey->since;
						
						$elementsCount++;
						$lst[$key] = $val;
					}
					
					$file_suitcase .= "<br/>";
					$file_suitcase .= table($lst, Lex::getKeys() . " <span class=\"badge\">$elementsCount</span>");
					$lst = [];
				}
				
				$file_suitcase .= "</body></html>";
				file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualSuitcase->name . ".html", $file_suitcase);
				$_filesCreatedCount++;
			}
		}
	}
}

//: CREATION DES FICHIERS DE TYPEDEFS
if (property_exists($data, "packages") && $data->packages != NULL) {
	foreach ($data->packages as $actualPackage) {
		if (property_exists($actualPackage, "typedefs") && $actualPackage->typedefs != NULL) {
			foreach ($actualPackage->typedefs as $actualTypedef) {
				fputs(STDOUT, "[$_apptitle] Creating file elements/" . $actualPackage->name . "." . $actualTypedef->name . ".html\n");
				$file_typedef = "<!DOCTYPE HTML><html><head><title>" . $actualTypedef->name . "</title><meta charset=\"utf-8\"/><link rel=stylesheet type=\"text/css\" href=\"../css/style.css\"/></head><body>";
				$file_typedef .= getHeaderMenu(true);
				$file_typedef .= "<ol class=\"breadcrumb\"><li><a href=\"" . $actualPackage->name . ".html\">" . $actualPackage->name . "</a></li><li class=\"active\">" . $actualTypedef->name . "</li></ol>";
				$file_typedef .= "<div class=\"page-header\"><h1>" . $actualTypedef->name . " <small>" . Lex::getTypedef() . "</small></h1></div>";
				
				if (property_exists($actualTypedef, "deprecated") && $actualTypedef->deprecated)
					$file_typedef .= "<div class=\"alert alert-danger\">" . Lex::getDeprecatedWarning() . "</div>";
				
				if (property_exists($actualTypedef, "description") && $actualTypedef->description != NULL && $actualTypedef->description != "")
					$file_typedef .= "<p>" . $actualTypedef->description . "</p>";
				
				$lst[Lex::getValeur()] = ObjectsFinder::renderObject($actualTypedef->value, ObjectsFinder::$TYPE_STD);
				if (property_exists($actualTypedef, "since") && $actualTypedef->since != NULL && $actualTypedef->since != "")
					$lst[Lex::getVersionMin()] = $actualTypedef->since;
				if (property_exists($actualTypedef, "author") && $actualTypedef->author != NULL && $actualTypedef->author != "")
					$lst[Lex::getAuteur()] = $actualTypedef->author;
				if (!empty($lst)) {
					$file_typedef .= "<br/>";
					$file_typedef .= table($lst, Lex::getInformations());
					$lst = [];
				}
				
				$file_typedef .= "</body></html>";
				file_put_contents("$output/elements/" . $actualPackage->name . "." . $actualTypedef->name . ".html", $file_typedef);
				$_filesCreatedCount++;
			}
		}
	}
}


// END
// on zip si voulu
if (array_key_exists("argv", $_SERVER) && sizeof($_SERVER['argv']) >= 5 && $_SERVER['argv'][4] == "-zip")
{
	fputs(STDOUT, "[$_apptitle] Compressing documentation\n");
	
	$za = new ZipArchive();
	if (!file_exists($output . ".zip"))
		$za->open($output . ".zip", ZipArchive::CREATE);
	else
		$za->open($output . ".zip");
	$za->addDir($output);
	$za->close();
}
fputs(STDOUT, "[$_apptitle] Documentation created.\n");
$eax = "";
for ($count = 0; $count <= strlen($_apptitle) + 3; $count++)
	$eax .= " ";
fputs(STDOUT, $eax . sprintf(Lex::getStatisticsMessage(), $_filesCreatedCount));
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredFunctionsCount() . " function(s),\n");
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredClassesCount() . " classe(s),\n");
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredInterfacesCount() . " interface(s),\n");
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredVariablesCount() . " variable(s),\n");
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredAnnotationsCount() . " annotation(s),\n");
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredPreprocessorConstantsCount() . " preprocessor constant(s),\n");
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredEnumsCount() . " enum(s),\n");
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredTypedefsCount() . " typedef(s),\n");
fputs(STDOUT, $eax . ObjectsFinder::getRegisteredPackagesCount() . " package(s).\n");
?>